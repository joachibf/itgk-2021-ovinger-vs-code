### **[Intro øving 5](../Intro_Øving5.md)**

<br>

# Vektorer

**Læringsmål:**

* Lister

**Starting Out with Python:**

* Kap. 7.1-7.3

<br>

## INTRODUKSJON

### **Representasjon av vektorer**
En liste kan brukes til å representere en matematisk vektor.
En av forskjellene mellom lister og vektorer er at lister kan inneholde elementer av forskjellige typer (f.eks både tall og strenger). I denne oppgaven skal du bruke lister til å implementere tredimensjonale vektorer, altså vektorer med tre elementer.

**En vektor x=(x,y,z) har lengde $\sqrt{x^{2}+y^{2}+z^{2}}$. Den kan multipliseres med tall, og vektormultipliseres med andre vektorer.**

Multiplikasjon med tall, også kalt skalarmultiplikasjon, ganger hver komponent i vektoren med et tall (skalar).

Prikkprodukt er multiplikasjon av to vektorer med samme lengde. Prikkprodukt og skalarmultiplikasjon er forklart i Eksempel 1.

Eksempel 1:


**Prikkprodukt:** <br>
$u*v = u1*v1 + v2*u2 + v3*u3$ , <br> hvor $v = [v1,v2,v3]$ og $u = [u1,u2,u3]$ <br>
Numerisk eksempel: <br>
$[3,7,1]*[2,-4,8]=3*2+7*(-4)+1*8=-14$ <br>

___
**Skalarmultiplikasjon:** <br>
$c*[v1,v2,v3] = [c*v1,c*v2,c*v3]$ <br>
Numerisk eksempel: <br>
$2*[3,7,1] = [6,14,2]$


<br>


<br>

## OPPGAVER
Alle deloppgaver skal løses her: *[Vektorer.py](Vektorer.py)* ! <br>
I denne oppgaven skal vi studere lister bestående av tall, dvs. en vektor. Videre skal vi studere prikkprodukt og skalering av vektorer. Gjennom oppgave a til f skal du lage et program som bruker forskjellige funksjoner til å gjøre forskjellige beregninger på vektorer.

## a)
Lag en funksjon som lager en liste med 3 elementer, hvor alle elementene er heltall f.o.m. -10 t.o.m. 10, og returnerer denne. Benytt deg av random-biblioteket.

<br>

## b)
Lag en funksjon som tar inn en liste/vektor og et navn til denne vektoren som argumenter. Funksjonen skal skrive ut vektoren på en vakker måte.

<br>

## c)
Lag en funksjon `scalar_mult(liste,skalar)` som tar inn en liste/vektor og en skalar som argumenter. Skalarmultipliserer vektoren og returnerer den nye vektoren, liste2. 

Det følger med en testkode til denne deloppgaven.

<br>

## d)
Lag en funksjon `vec_len(vektor)` som tar inn en vektor som argument, og regner ut og returnerer dens lengde. 

Det følger med en testkode til denne deloppgaven.

<br>

## e)

Lag en funksjon `vector_dot_product(vec1,vec2)` som tar inn to vektorer som argumenter og returnerer indreproduktet/prikkproduktet av de to vektorene. 
Du kan gå ut ifra at vektorene har lik lengde. 

Det følger med en testkode til denne deloppgaven.

<br>

## f)
Lag en main-funksjon som:

1. Oppretter en vektor med 3 elementer (vec1).
2. Skriver ut vektoren på fin form.
3. Tar inn en skalar fra bruker. (Skal kunne ta inn både heltall og flyttall.)
4. Skriver ut lengden til vektoren før og etter skalering med to desimaler.
5. Skriver ut forholdet mellom de to lengdene. (Forholdet bør være lik skalaren.)
6. Skriver ut prikkproduktet mellom vektoren før skalering og vektoren etter skalering. Dvs. prikkproduktet mellom $vec1$ og $(vec1*skalar)$, altså $vec1*(vec1*skalar)$.

Eksempel på kjøring:

>*vec1 = [9, 3, 0] <br>
Skriv inn en skalar:* 2.5 <br>
*Lengden før skalering er: 9.49 <br>
Lengden etter skalering er: 23.72 <br>
Forholdet mellom lengden før og etter skalering er: 2.5 <br>
Prikkproduktet av [9, 3, 0] og [22.5, 7.5, 0.0] er: 225.0* <br>

