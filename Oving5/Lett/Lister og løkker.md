### **[Intro øving 5](../Intro_Øving5.md)**

<br>

# Lister og løkker

**Læringsmål:**

* Lister

* Løkker

**Starting Out with Python:**

* Kap. 7.7


<br>

## INTODUKSJON

### **Iterering gjennom en liste**
For å iterere/gå gjennom elementene i en liste brukes løkker, og dette gjøres hovedsakelig på to ulike måter:

1) For-løkker, ved å bruke range()-funksjonen og indekser.

Eksempel:
```python
liste = [1,2,3,4,5]
lengde = len(liste)   #5
for x in range(lengde):
    print(liste[x])
```
2) For-in-løkker, som henter ut ett og ett element av en liste.

Eksempel:
```python
liste = [1,2,3,4,5]
for x in liste:
    print(x)
```
Disse metodene brukes om hverandre, og det er lurt å kunne bruke begge. Begge gir samme output. 

<br>

<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Lister og løkker.py](Lister%20og%20løkker.py)* !

## a)
Lag en liste **number_list** med alle heltallene fra 0 til og med 99.

**Hint:** <br>
For å legge til et nytt **element** til en liste (**my_list**) kan du bruke den innebygde funksjonen "append" slik: my_list.append(element).

<br>

## b)
Summer sammen alle tall i listen som er delelig på 3 eller 10. (Summen skal bli 1953)

<br>

## c)
Bytt plass på alle nabo partall og oddetall i listen. (listen skal nå være: [1, 0, 3, 2, 5, 4, 7, 6, 9, 8,..., 98])

<br>

## d)
Lag en ny liste **reversed_list** som inneholder elementene i **number_list** i motsatt rekkefølge. (Merk: uten å bruke innebygde funksjoner i Python)

Dersom du nå printer listen skal du få:
    
```python
[98, 99, 96, 97, 94, 95, 92, 93, 90, 91, 88, 89, 86, 87, 84, 85, 82, 83, 80, 81, 78, 79, 76, 77, 74, 75, 72, 73, 70, 71, 68, 69, 66, 67, 64, 65, 62, 63, 60, 61, 58, 59, 56, 57, 54, 55, 52, 53, 50, 51, 48, 49, 46, 47, 44, 45, 42, 43, 40, 41, 38, 39, 36, 37, 34, 35, 32, 33, 30, 31, 28, 29, 26, 27, 24, 25, 22, 23, 20, 21, 18, 19, 16, 17, 14, 15, 12, 13, 10, 11, 8, 9, 6, 7, 4, 5, 2, 3, 0, 1]
```