### **[Intro øving 5](../Intro_Øving5.md)**

<br>

# Gangetabell og lister


**Læringsmål:**

* Løkker
* Lister

**Starting Out with Python:**

* Kap. 7: Lists and Tuples

<br>

## OPPGAVER
Alle deloppgaver skal løses her: *[Gangetabell og lister.py](Gangetabell%20og%20lister.py)* !

<br>

## a)
Lag en funksjon `separate(numbers, threshold)` som tar inn to argumenter:

* numbers: en liste med heltall
* threshold: et heltall
Funksjonen skal returnere to lister: den første listen skal inneholde alle elementer fra **numbers** som er mindre enn **threshold**, og den andre listen skal inneholde alle elementer fra **numbers** som er større enn eller lik **threshold**.

 
En matrise er et rektangulært sett av elementer ordnet i rader og kolonner. Radene er de horisontale linjene, og kolonnene er de vertikale linjene.

I Python representerer vi dette med lister som inneholder lister.  Elementet øverst til venstre i eksempelet under (med verdi 1) er i rad 1, kolonne 1.

```python
matrise = [
[1, 2, 3],
[4, 5, 6],
[7, 8, 9]]
```

Elementene i matrisen kan hentes ut på følgende måte: **matrise[radnr][kolnr]**. 

*Husk at radnummer og kolonnenummer er null-indeksert (begynner på 0) i Python i motsetning til matematisk notasjon der de er 1-indeksert!*


<br>

 ## b)
 Lag en funksjon `multiplication_table(n)`som tar inn et heltall n som parameter og returnerer gangetabellen fra 1 til n som en matrise med n rader og n kolonner.

 Har du implementert funksjon riktig skal koden:

```python
print(multiplication_table(4))
```
printe ut følgende matrise:

>[[1, 2, 3, 4], <br>
 [2, 4, 6, 8], <br>
 [3, 6, 9, 12], <br>
 [4, 8, 12, 16]]

<br>

<br> 

**Hint:** <br>
Man kan legge til elementer (som også kan være lister) på slutten av en liste med **liste.append(ele
ment)**.
