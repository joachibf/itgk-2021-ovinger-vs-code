### **[Intro øving 5](../Intro_Øving5.md)**

<br>

# Teoridelen på eksamen

**Læringsmål:**

* Lister
* funksjoner

**Starting Out with Python:**

* Kap. 7

<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Teoridelen på eksamen.py](Teoridelen%20på%20eksamen.py)* ! <br>

I denne oppgaven skal du sammenligne to lister og studere hvor like de er hverandre. 

25% av eksamen er flervalgsoppgaver, og her skal vi anta at det alltid vil være 20 oppgaver. Riktige svar for oppgavene er som følger:
Riktige svar| | | |
---|---|---|---
1.A|6.A|11.D|16.A
2.C	|7.B|	12.A|	17.B
3.B|	8.A	|13.C	|18.A
4.D	|9.C|	14.C|	19.C
5.A	|10.A|	15.B|	20.D

<br>

<br>

## a)
Lag en liste `fasit` som inneholder de korrekte svarene.

<br>

## b)
Lag en funksjon `sjekk_svar()` som tar inn studentens_svar som argument. studentens_svar er en liste som inneholder en students svar på oppgavene. Ved å sammenligne studenten sine svar med de riktige svarene, skal funksjonen returnere hvor mange prosent av oppgavene studenten klarte. 

Denne oppgaven har en testkode vedlagt.


