### **[Intro øving 5](../Intro_Øving5.md)**

<br>

# Lett og blandet om lister

**Læringsmål:**

* Lister

**Starting Out with Python:**

* Kap. 7.1-7.3


## INTRODUKSJON

Les om innebygde funksjoner i Python [her](https://docs.python.org/3/library/functions.html), og mer om lister i Python [her](https://docs.python.org/3/tutorial/datastructures.html). Merk at kun noen av disse er pensum, og det vil bli utdelt et nokså utfyllende "formelark" på eksamen.

### **Sum og lengde av en liste**
sum(listen) returnerer den totale summen av alle elementene i listen, for eksempel sum([1,2,3,4]) = 1+2+3+4 = 10.

len(listen) returnerer antall elementer i listen, for eksempel len([1,2,3,4])=4.

<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Lett og blandet om lister.py](Lett%20og%20blandet%20om%20lister.py)* ! Det følger med testkode til alle oppgavene.

## a)
Lag en funksjon **is_six_at_edge()**, som tar inn en liste med heltall som parameter og returnerer **True** dersom **6** er det første eller det siste elementet i listen. Funksjonen returnerer ellers **False**.
NB! Funksjonen skal også returnere True hvis lista starter **og** slutter på 6. 

<br>

## b)
Bruk den innebygde funksjonen [sum()](https://docs.python.org/3/library/functions.html#sum), samt len() for å lage en funksjon average() som tar inn en liste med tall som parameter og returnerer gjennomsnittsverdien til tallene i listen.

<br>

## c)
Bruk den innebygde metoden [**.sort()**](https://docs.python.org/3/howto/sorting.html) for å lage en funksjon **median()** som tar inn en liste og finner medianen av listen og returnerer den. Husk at medianen er det midterste elementet i en sortert rekkefølge av listens elementer.  
*NB! Du kan anta at det er et oddetall antall elementer i listen!*

**Hint** <br>
Ved å skrive **my_list.sort()** sorterer du elementene i listen **my_list**.