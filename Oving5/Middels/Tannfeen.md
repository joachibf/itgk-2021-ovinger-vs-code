
### **[Intro øving 5](../Intro_Øving5.md)**

<br>

# Tannfeen

**Læringsmål:**

* Lister
* Løkker

**Starting Out with Python:**

* Kap. 7.8


<br>

## OPPGAVER
Oppgaven skal løses her: *[Tannfeen.py](Tannfeen.py)* !

Denne oppgaven har en vedlagt testkode i .py filen. Kjør denne for å se om du har løst oppgaven korrekt.

*Overformynderiet for tannfeer og python-programmerere* har vedtatt at istedenfor å betale 20 kroner for hver enkelt tann et barn mister og putter i vannglasset sitt, er de nå nødt til å betale per gram tann. Det ble bestemt at ett gram tann er verd 1 krone. Tannfeer vil helst bruke så store mynter de kan (i verdi). Gyldige mynter er [20,10,5,1].

Siden tannfeen besøker så mange, vil de ha et program som skal kunne regne ut dette fra lister. Det vil si at programmet tar inn en liste av tannvekter, og får tilbake en liste (av lister) over hvilke mynter som trengs for tennene.

Eksempel:

```python
teeth = [53,28]
```

$->$$[[2,1,0,3],[1,0,1,3]]$

Eksempelet over vil si at for 53g vil man få 2 stk 20kr, 1 stk 10kr, 0 stk 5kr og 3 stk 1kr, og for 28g vil man få 1 stk 20kr, 0 stk 10kr, 1 stk 5kr og 3 stk 1kr.

**Oppgave:** Lag programmet. Funksjonen som tar inn en liste og returnerer antall kroner skal hete `antKroner()`.