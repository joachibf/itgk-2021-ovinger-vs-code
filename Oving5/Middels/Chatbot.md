### **[Intro øving 5](../Intro_Øving5.md)**

<br>

# Chattebot

**Læringsmål:**

* Lister

**Starting Out with Python:**

* Kap. 7.1-7.3

<br>

## OPPGAVE
Oppgaven skal løses her: *[Chatbot.py](Chatbot.py)* ! I filen ligger det et skjelett som dere skal løse oppgaven i.



I denne oppgaven skal du lage et program som simulerer en datavenn du kan snakke med, en såkalt "chatbot".

**Oppgave:** Skriv koden som mangler for å fullføre programmet.

Hovedfunksjonen til programmet er følgende:

Spørre brukeren om navnet.
Stille et tilfeldig spørsmål fra en liste av spørsmål til brukeren der navnet inngår.
La brukeren få svare.
Stille et tilfeldig oppfølgingsspørsmål, der svaret fra brukeren er med.
Gi en tilfeldig kommentar til brukeren.
Programmet skal avsluttes ved at brukeren gir svaret “hade”. Kildekoden er godt dokumentert med kommentarer og alle steder som mangler kode er markert med “MANGLER KODE HER!!!”. 

Husk: **pass** i starten av while-løkken må fjernes når dere begynner å skrive koden deres. 


**Eksempel på kjøring:**


>*Hei , Jeg heter HAL og vil gjerne snakke med deg! <br>
 Ikke start svar med stor bokstav og bruk hele setninger. <br>
 Skriv 'hade ' hvis du vil avslutte   <br>*
$______________________________________$ <br> <br>
*Hva heter du?* Bob Bernt<br>
*Hvorfor heter du  Bob Bernt?<br>
 Svar :* mamma liker det navnet<br>
*Hvilke tanker har du om  mamma liker det navnet?<br>
 Svar :* litt ekkelt<br>
*Så bra  Bob Bernt.<br>
Hva gjør deg trist  Bob Bernt?<br>
 Svar :* ikke noe mer sjokolade<br>
*Hvorfor sier du  ikke noe mer sjokolade?<br>
 Svar :* hæ?<br>
*Så dumt da  Bob Bernt.*<br>
 *Hva tenker du om framtida  Bob Bernt?<br>
 Svar :* hade <br>
*Hva mener du med hade? <br>
 Svar :* ku <br>
*Blir trist av det du sier  Bob Bernt.*<br>
