### **[Intro øving 5](../Intro_Øving5.md)**

<br>

# Lotto

**Læringsmål:**

* Lister
* Tilfeldige tall

**Starting Out with Python:**

* Kap. 7


<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Lotto.py](Lotto.py)* !

I denne oppgaven lage du lage en lottosimulator. 

Reglene er som følger:

* Det trekkes ut 7 lottotall og 3 tilleggstall fra og med 1 til og med 34. Alle tallene som trekkes skal være unike.
* Premier deles ut basert på følgende tabell:

Premiergruppe|Premie (kr)
:---|---
7 rette	|2 749 455
6 rette + 1 tilleggstall	|102 110
6 rette	|3 385
5 rette	|95
4 rette + 1 tilleggstall	|45


<br>

## a)
Lag en liste som heter `numbers` og som inneholder alle heltall fra og med 1 til og med 34.


<br>

## b)
Lag en liste som heter `myGuess` med 7 tall. Denne listen inneholder tallene som du tipper.

<br>

## c)
Lag en funksjon `drawNumbers(numbers, n)` som tar inn `numbers` og `n` som argument og som trekker ut `n` tall ut av listen `numbers` og legger de i en egen liste.  
For å gjøre ting tilfeldig: `import random` og `random.randint(a,b)` gir tilfeldige tall fra og med a til og med b. 

Eksempel på kjøring:

```python
print(drawNumbers(numbers, 7))
```
$->$ [16, 33, 5, 20, 7, 4, 8]

<br>

**Hint:** <br> Bruk funksjonene `pop()` og `append()` for å fjerne og legge til elementer i en liste. Husk at pop fjerner et element i en indeks i lista, den fjerner ikke tallet. Så numbers.pop(rand_num) fjerner elementet på indeks rand_num - altså hvis rand_num er 13 fjernes tallet på indeks 13, ikke tallet 13!

<br>

## d)
Lag funksjonen `compList(list1, list2)` som sammenligner to lister med tall. Antall like tall i listene skal returneres.

Eksempel på kjøring:

```python
print(compList(drawNumbers(numbers,7),myGuess))
```
$->$ 1

<br>

## e)
Lag en funksjon `winnings()`som tar inn antall like tall og like tilleggstall, og returnerer premien du har vunnet. <br>

Eksempel på kjøring:

```python
print(winnings(7,1))
```
$->$ 2749455

<br>

```python
print(winnings(5,2))
```
$->$ 95

<br>

<br>

## f)
Funksjonene skal settes sammen i main() slik at dette blir en fullverdig lottosimulator (for en lottorekke). Tallene du skal trekke ut (både lottotallene og tilleggstallene) kan legges i samme liste. Funksjonen `compList` kan da sammenligne de første 7 tallene, og så de siste 3 tallene, for å finne ut hvor mange like tall du har. main() skal returnere hvor mye du har tjent eller mest sannsynlig tapt på denne lottorekken. Dersom en lottorekke kosten 5 kroner, vil -5 returneres dersom winnings() er 0. Hvis du er heldig og Winnings() blir 95 skal 90 returneres fra main(). 

**Husk at du kan bruke alle funksjoner du har definert over!**

<br>

## g) Frivillig
Finn ut hvor mye man har vunnet etter å ha tippet en million ganger. Anta at premiepotten er det samme hver uke, og at en lottorekke koster 5 kroner.
