from math import sqrt
print("Varierte funksjoner")

print("Oppgave a)")
#TODO løs oppgave a her:

def divisable(a, b):
    return False if b==0 else True if a%b==0 else False


# TESTKODE FOR divisable
print("Er",divisable(10,3), "skal være false")
print("Er",divisable(10,5), "skal være true")



print("Oppgave b)")
#TODO løs oppgave b her:
def isPrime(a):
    for b in range(2,a):
        if divisable(a, b):
            return False
    return True



# TESTKODE FOR isPrime
print("Er",isPrime(11), "skal være true")
print("Er",isPrime(15), "skal være false")



print("Oppgave c)")
#TODO løs oppgave c her:
def isPrime(a):
    for b in range(2,(round(sqrt(a)+0.5))):
        if b%2==0:
            continue
        if divisable(a, b):
            return False
    return True

print("Er",isPrime(11), "skal være true")
print("Er",isPrime(15), "skal være false")