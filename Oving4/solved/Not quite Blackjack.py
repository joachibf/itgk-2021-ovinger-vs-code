from hashlib import new
import random
from typing import Optional

deck = [] #Globals capitalised in accordance with pep8

def generate_deck():
    global deck
    cards = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"] #Ace through king
    suits = ["S", "D", "C", "H"] #Spades, Diamonds, Clubs and Hearts respectively
    deck = []
    for card in cards:
        deck.extend([f"{s}{card}" for s in suits])

print("Not quite Blackjack!")

def draw_card(i: Optional[int] = 1):
    """Draws and returns a random card from the deck, removing it from the deck in the process"""
    cards = []
    for j in range(i):
        card = random.choice(deck)
        deck.remove(card)
        cards.append(card)
    return cards

def calculate_score(hand=None):
    score = 0
    for card in hand:
        if int(card[1:])>9:
            score+=10
        elif int(card[1:])==1:
            score += 11 if (score+11)<=21 else 1
        else:
            score += int(card[1:])
    return score

#TODO løs oppgaven her:
def blackjack():
    dealers_hand = []
    your_hand = []
    dealers_score = 0
    your_score = 0
    while True:
        if False:
            print(deck)
            print(dealers_hand)
            print(your_hand)
        if len(dealers_hand)==0 and len(your_hand)==0:
            dealers_hand = []
            your_hand = []
            dealers_hand.extend(draw_card())
            print(f"Dealers cards are {dealers_hand[0]} and ?")
            dealers_score = calculate_score(dealers_hand)
            print(f"Their score is: {dealers_score}")
            your_hand.extend(draw_card(2))
            your_score = calculate_score(your_hand)
            print(f"Your score is: {your_score}")
        if your_score==21 & dealers_score!=21:
            print("Winner Winner Chicken Dinner!")
            exit()
        elif your_score>21:
            print("Bust! You lost.")
            exit()
        else:
            new_card = input("Do you want another card? (Y/N) ")
            if new_card.lower() == 'y':
                new_card = draw_card().pop()
                your_hand.append(new_card)
                print(f"You drew {new_card}")
                your_score = calculate_score(your_hand)
                print(f"Your score is: {your_score}")
            elif new_card.lower() == 'n':
                new_card = draw_card().pop()
                dealers_hand.append(new_card)
                dealers_score = calculate_score(dealers_hand)
                print(f"Dealers score is: {dealers_score}")
                if dealers_score > your_score:
                    print("You lost")
                    exit()
                elif dealers_score == your_score:
                    print("Tie!")
                    exit()
                else:
                    print("You win")
                    exit()









if __name__ == '__main__':
    generate_deck()
    blackjack()