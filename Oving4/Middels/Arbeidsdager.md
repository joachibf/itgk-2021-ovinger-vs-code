
### **[Intro øving 4](../Intro_Øving4.md)**

<br>

# Arbeidsdager

**Læringsmål:**
- Funksjoner 
- Betingelser
- Løkker

**Starting Out with Python:**
- Kap. 3.1-3.2
- Kap. 4.3
- Kap. 5.3
- Kap. 5.5
- Kap. 5.8


## OPPGAVER 
Alle deloppgaver skal besvares her: *[Arbeidsdager.py](Arbeidsdager.py)* !

Et vanlig år består av 52 hele uker og én dag, til sammen 365 dager. Det vil si at hvis ett år starter på en mandag, starter neste år på en tirsdag. Skuddår har en dag ekstra. Dvs. hvis et skuddår starter på en mandag, starter neste år på en onsdag. 1. januar 1900 var en mandag. Skuddår er litt finurlig definert. I .py filen er det lagt ved en funksjon du kan bruke for å bestemme om et år er et skuddår. 


## a)

Lag først en funksjon `weekday_newyear(year)` som tar inn et årstall, og returnerer hvilken ukedag året starter på. Ukedager representeres med heltall, dvs. mandag = 0, tirsdag = 1, ..., søndag = 6. 

*Merk: Deloppgaven skal løses uten å bruke Pythons innebygde datofunksjoner.*

Skriv deretter ut årstall med tilhørende første ukedag i tekstlig format for år 1900 til og med 1919 ved å benytte funksjonen `weekday_newyear(year)`.

Hvis du har løst oppgaven korrekt, skal output se slik ut:


>1900 man <br>
1901 tir <br>
1902 ons <br>
1903 tor <br>
1904 fre <br>
1905 son <br>
1906 man <br>
1907 tir <br>
1908 ons <br>
1909 fre <br>
1910 lor <br>
1911 son <br>
1912 man <br>
1913 ons <br>
1914 tor <br>
1915 fre <br>
1916 lor <br>
1917 man <br>
1918 tir <br>
1919 ons <br>
 
<br>

## b)
Lag funksjonen `is_workday(day)` som tar inn en ukedag, og returnerer `True` om ukedagen er arbeidsdag, og `False` ellers. (Alle hverdager er arbeidsdager. Lørdag og søndag er ikke arbeidsdager. For eksempel skal `is_workday(2)` returnere `True`, mens `is_workday(5)` skal returnere `False`.

<br>

## c)

Lag først funksjonen `workdays_in_year(year)` som tar inn et årstall og skriver ut antall arbeidsdager i det gitte året. (Vi ser bort ifra helligdager som faller på arbeidsdager, dvs. at bl.a. langfredag vil telles som en arbeidsdag.) 

Husk at skuddår har en ekstra dag i februar. 

Skriv deretter ut antall arbeidsdager for årene 1900 til og med 1919.


**Hint:** Benytt deg av tidligere lagde funksjoner for å slippe å skrive mye kode på nytt.

Har du gjort alt riktig skal koden din gi følgende output:


>1900 har 261 arbeidsdager <br>
1901 har 261 arbeidsdager <br>
1902 har 261 arbeidsdager <br>
1903 har 261 arbeidsdager <br>
1904 har 261 arbeidsdager <br>
1905 har 260 arbeidsdager <br>
1906 har 261 arbeidsdager <br>
1907 har 261 arbeidsdager <br>
1908 har 262 arbeidsdager <br>
1909 har 261 arbeidsdager <br>
1910 har 260 arbeidsdager <br>
1911 har 260 arbeidsdager <br>
1912 har 262 arbeidsdager <br>
1913 har 261 arbeidsdager <br>
1914 har 261 arbeidsdager <br>
1915 har 261 arbeidsdager <br>
1916 har 260 arbeidsdager <br>
1917 har 261 arbeidsdager <br>
1918 har 261 arbeidsdager <br>
1919 har 261 arbeidsdager <br>



