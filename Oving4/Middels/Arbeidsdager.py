print("Arbeidsdager!")

def is_leap_year ( year ):
    if year % 400 == 0:
        return True
    elif year % 100 == 0:
        return False
    elif year % 4 == 0:
        return True
    return False


print("Oppgave a)")
#TODO løs oppgave a her:
def weekday_newyear(year):
    days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
    counter = -1
    for yr in range(1900,year+1):
        if is_leap_year(yr-1):
            counter+=2
        else:
            counter+=1
    return days[counter%7]

for i in range(1900,1920):
    print(i, weekday_newyear(i))




print("Oppgave b)")
#TODO løs oppgave b her:
def is_workday(day):
    return True if day<5 else False

print(5, is_workday(5))
print(2, is_workday(2))

print("Oppgave c)")
#TODO løs oppgave c her:

def workdays_in_year(year):
    days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
    cnt = 365 if not is_leap_year(year) else 366
    starts = days.index(weekday_newyear(year))
    workdays = 0
    for i in range(0,cnt):
        if is_workday((starts+i)%7):
            workdays+=1
    return workdays

for i in range(1900,1920):
    print(i, "har", workdays_in_year(i),"arbeidsdager")