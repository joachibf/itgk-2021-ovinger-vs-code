# Øving 4

**Læringsmål:**
- Funksjoner
- Løkker
- Betingelser

**Starting Out with Python:**
- Kap. 5: Functions

**Theory book:**
- Kap. 7: Representing Information Digitally
- Kap. 8: Representing Multimedia Digitally
- Kap. 12: Representing Integers
 

## Godkjenning:

For å få godkjent øvingen må du gjøre  **4 av 11** oppgaver.

Alle oppgavene skal demonstreres til en læringsassistent på sal. I oppgaver der du skriver programkode skal også denne vises fram. Lykke til!

Oppgave|Tema|Vanskelighetsgrad
---|---|--
[Grunnleggende om funksjoner](Lett/Grunnleggende%20om%20funksjoner.md)|Funksjoner| Lett
[Varierte funksjoner](Lett/Varierte%20funksjoner.md)|Funksjoner| Lett
[Lokale variabler](Lett/Lokale%20variabler.md)|Funksjoner| Lett
[Globale variabler](Lett/Globale%20variabler.md)|Funksjoner| Lett
[Euklids algoritme](Lett/Euklids%20algoritme.md)|Funksjoner, Algoritmer| Lett
[Primtall](Lett/Primtall.md)|Funksjoner, Løkker| Lett
[Multiplikasjon](Lett/Multiplikasjon.md)|Funksjoner, Løkker| Lett
[Den store spørreundersøkelsen](Middels/Den%20store%20spørreundersøkelsen.md)|Funksjoner, Løkker|Middels
[Arbeidsdager](Middels/Arbeidsdager.md)|Funksjoner, Løkker|Middels
[Sekantmetoden](Middels/Sekantmetoden.md)|Funksjoner, Løkker|Middels
[Not quite Blackjack](Middels/Not%20quite%20Blackjack.md)|Funksjoner, Løkker, Betingelser|Middels
