### **[Intro øving 4](../Intro_Øving4.md)**

<br>

# Varierte funksjoner

**Læringsmål:**
- Kodeforståelse
- Funksjoner

**Starting Out with Python:**
- Kap. 5.1, 5.2, 5.5, 5.8

Oppgaven *grunnleggende om funksjoner* dreier seg mest om å omstrukturere eksisterende kode. I denne oppgaven skal du få prøve å skrive funksjoner helt fra grunnen av.

## OPPGAVER

Alle deloppgaver skal besvares her: *[Varierte funksjoner.py](Varierte%20funksjoner.py)* !

## a)
Absoluttverdi er en funksjon med mange nyttige anvendelser, bl.a. i statistisk analyse (f.eks. utregning av standardavvik).

Python har allerede funksjonen `abs()`. Men siden dette er en enkel funksjon å lage, later vi her som om den ikke fins, slik at det er behov for å skrive den.

Kall din funksjon `absol()` så ikke navnet kolliderer med den eksisterende funksjonen. Reglene for `absol(x)` skal da være:

- hvis x >= 0, er absoluttverdien til x identisk med x
- hvis x < 0, er absoluttverdien -x (minus minus gir pluss, absoluttverdi skal alltid være positiv)


<br>

## b)
Skriv en funksjon som får inn hastighet i knop og returnerer hastighet i km/t ved hjelp av følgende opplysninger:

- 1 knop er 0.514444444 m/s
- 1 m/s er 3.6 km/t

Kall funksjonen for `knop2km_t`.

<br>

## c)
 I England og USA har det vært vanlig å oppgi personers høyde i fot og tommer (feet, inches - såkalte **imperial** måleenheter).

Lag en funksjon med navn `imp2cm()` som får inn som parametrenen antall fot og antall tommer og returnerer antall cm som dette tilsvarer.

Til dette kan man bruke følgende opplysninger:

en fot tilsvarer 12 tommer
en tomme tilsvarer 2.54 cm
Funksjonen skal returnere antall cm avrundet til nærmeste heltall.

<br>

## d)
Farger for utskrift på papir angis i CMYK, med 4 flyttall mellom 0-1 for hvor mye cyan (C), gult (Y), magenta (M) og sort (K) printerblekk.

Når farger skal vises på skjerm, angis de i RGB, dvs. som 3 heltall 0-255 som angir intensiteten av rødt (R), grønt (G) og blått (B) lys.

"Nina" har jobbet som designer av brosjyremateriell på papir og vet om mange kule farger som hun kjenner CMYK-verdiene for.

Kundene går i økende grad over til nettbasert infomateriell, så hun ønsker nå et program for å konvertere fra CMYK til RGB.

Gitt verdier for C, M, Y, K som flyttall mellom 0 og 1, er formlene for konvertering slik:

- R = 255 * (1-C) * (1-K)
- G = 255 * (1-M) * (1-K)
- B = 255 * (1-Y) * (1-K)
Mesteparten av programmet er gjort ferdig av en annen programmerer, men funksjonen **cmyk2rgb()** for selve konverteringen mangler.

***Skriv inn den manglende koden, kjør deretter koden og se om den virker.***

<br>

**Hint 1:** Du må navngi funksjonen akkurat `cmyk2rgb` hvis den skal virke med den koden som er gitt på forhånd, siden dette navnet brukes i et kall midt nede i koden.

Funksjonen må ha et antall parametre som stemmer med antallet argumenter i kallet cmyk2rgb(C, M, Y, K) -  dvs. 4 parametre.



**Hint 2:** Siden det står R, G, B = cmyk2rgb... i skriptet, må det komme **3 verdier** fra funksjonen for at alle disse variablene skal få innhold.

Altså trenger funksjonen å returnere 3 verdier, ikke  bare 1 som vi har sett tidligere.

Dette er imidlertid ikke spesielt vanskelig - å returnere flere verdier fra en funksjon gjøres simpelthen ved å liste opp flere returverdier med komma mellom.

F.eks., `return x, y, z` vil  returnere verdiene til 3 variable x, y, z fra en funksjon (men du kan godt gi dine variable litt mer intelligente navn)

 

**Hint 3:** C, M, Y, K  inneholder flyttall mellom 0 og 1. Utregningene med formlene  gitt over vil dermed også gi flyttall som resultat.

Du trenger imidlertid heltall 0-255 som verdier for R, G, B.