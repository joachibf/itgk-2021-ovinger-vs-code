### **[Intro øving 4](../Intro_Øving4.md)**

<br>

# Multiplikasjon

**Læringsmål:**
- Løkker
- Funksjoner

**Starting Out with Python:**
- Kap. 4.2-4.3


<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Multiplikasjon.py](Multiplikasjon.py)* !

<br>

## a)
Skriv en funksjon som tar inn en toleransegrense `tol` og returnerer `prod` og `count`, hvor `prod` er definert som:

$(1+\frac{1}{1^{2}})(1+\frac{1}{2^{2}})(1+\frac{1}{3^{2}})...$

og `count` er antall iterasjoner som har blitt kjørt.

Avslutt iterasjonen når endringen i produktet er mindre enn toleransegrensen `tol`. 

Skriv ut verdien og hvor mange iterasjoner som trengs. Hvis `tol` er 0.01 skal programmet skrive ut følgende:

>*Produktet ble 3.49 etter 19 iterasjoner.*

<br>

## b) FIVILLIG OG VANSKELIG OPPGAVE
Denne oppgaven krever at du bruker rekursjon, det vil si at funksjonen "kaller" på seg selv. Dette er en mye brukt programmeringsteknikk som vil bli gjennomgått senere i kurset. 

Lag en ny funksjon som gjør det samme som a) rekursivt. Avslutt rekursjonen når

$(1+\frac{1}{k^{2}}) < 1 + tol$

Skriv også ut rekusjonsdybden

**Eksempel på kjøring:**


>*Skriv inn tol:* 0.01 <br>
*Rekursjonsdybden er 10 <br>
Produktet ble: 3.37* <br>


**Merk:** den rekursive funksjonen i b) vil ikke gi samme svar som a) pga. at betingelsene som blir brukt er forskjellige.