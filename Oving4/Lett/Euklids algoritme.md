### **[Intro øving 4](../Intro_Øving4.md)**

<br>

# Euklids algoritme

**Læringsmål:**
- Funksjoner
- Algoritmer
- Løkker

**Starting Out with Python:**
- Kap. 5.2-5.5

<br>

## INTRODUKSJON


Et vanlig problem i matematikken er å finne den største felles divisoren av to tall. I denne oppgaven skal du lage en funksjon som finner dette tallet, og så bruke funksjonen til å forkorte brøker. 

Eksempelvis vil det største tallet som deler både 20 og 30 være 10, brøken 20/30 kan derfor forkortes til 2/3 ved å dele på 10 i både teller og nevner. 

Dette problemet kan løses ved hjelp av Euklids algoritme. Pseudokode for Euklids algoritme er som følger: 


**1.** Gitt inputparametere a og b, begge heltall. <br>
**2.** Gjenta så lenge b ikke er 0: <br>
**3.** Tilegn den nåværende verdien av b til en ny variabel gammel_b. <br>
**4.** Tilegn til b resten etter a dividert på b. <br>
**5.** Tilegn til a verdien av gammel_b. <br>
**6.** Returner a som resultat av algoritmen <br>

<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Euklids algoritme.py](Euklids%20algoritme.py)* !

## a)
Lag en funksjon `gcd` (Greatest Common Divisor) som tar inn to tall, utfører Euklids algoritme, og returnerer den største felles divisoren. For eksempel skal `gcd(30,10)` returnere svaret `10`

<br>

## b)
Du skal nå bruke gcd-funksjonen til å forkorte brøker. 

Lag en funksjon reduce_fraction som tar inn to positive heltall `a` og `b` og bruker gcd-funksjonen definert i forrige deloppgave for å finne største felles divisor `d`. 

Funksjonen skal returnere `a/d` og `b/d`.

<br>

**Tips**:<br>
- For å returnere to variabler fra en funksjon separerer man returverdiene med komma: `return var_a, var_b`

- Man kan også tilegne verdier til to variabler på samme kodelinje, dette gjøres ved å skrive: `a, b = funksjon_med_to_returverdier()` der funksjonen kalles.


