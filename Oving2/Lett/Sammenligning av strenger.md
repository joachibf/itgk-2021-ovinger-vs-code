### **[Intro øving 2](../Intro_Øving2.md)**

<br>

# Sammenligning av strenger

**Læringsmål:**
- Betingelser
- Kodeforståelse

**Starting Out with Python:**
- Kap. 3.2-3.3

## INTRODUKSJON

**Generelt om sammenlignig av strenger**

Når man sammenligner to strenger i Python vil strengene sammenlignes karakter for karakter. F.eks. vil et program som sammenligner strengene `"Ola"` og `"Ole"` og sjekker om de er like, først sammenligne `'O'` mot `'O'`, så `'l'` mot `'l'`, og til slutt `'a'` mot `'e'`. Det er først ved sammenligningen av `'a'` mot `'e'` at det vil bli returnert `False`. <br>
Om man derimot sammenligner navnene Markus og Marcus, vil testen av betingelsen returnere `False` når `'k'` blir sammenlignet med `'c'`. Det betyr at `'u'`  mot `'u'` og `'s'` mot `'s'` ikke blir sjekket. <br>
Python skiller mellom store og små bokstaver, og en stor A er ikke det samme som en liten a. Dette kommer av at tegnene i strengene lagres som tall, og det er disse tallene som sammenlignes. I ASCII er karakterene A til Z representert med tallene 65 til 90, og karakterene a til z er representert med tallene 97 til 122.

"Ann" == "Anne" -> False <br>
"Anders" ==  "Anders" -> True <br>
"Anders" == "anders" -> False

I noen tilfeller vil det være ønskelig at programmet ikke tar hensyn til om det er store eller små bokstaver i ordene. Da kan vi benytte oss av noen innebygde funkjsoner:

**str.lower()** <br>
Returnerer en kopi av strengen bestående av kun små bokstaver. Dvs., om du har en variabel `a = "DrUer"`, vil `a.lower()` gi strengen `"druer"`. Men husk, `a.lower()` endrer ikke på variabelen `a`, så man må enten lage en ny variabel `b = a.lower()` og bruke variabelen `b` i sjekken, eller så kan man bruke `a.lower()` direkte i sjekken i stedet for `a`.

Eksempel som viser bruk av `str.lower()`:


```python
a = "DrUer"
  
print(a)        #Output -> "DrUer"
a.lower()       #retunerer strengen "druer", men endrer ikke på variabelen a

print(a)        #Output -> "DrUer"
print(a.lower())    #Output -> "druer"
```

**str.upper()** <br>
Fungerer på samme måte som str.lower, men returnerer en kopi av strengen bestående av kun store bokstaver. 

<br>

**Bruk av æ, ø og å**
I Python3 brukes Unicode-tabellen til representasjon av bokstaver, og mens verdiene for bokstavene a-z er de samme som i ASCII-tabellen, er det litt annerledes for æ, ø og å. Disse kommer i en litt annen rekkefølge enn vi er vant med og har følgende verdier:


`'å'` - 229  
`'Å'` - 197  
`'æ'` - 230  
`'Æ'` - 198  
`'ø'` - 248  
`'Ø'` - 216  

Legg merke til at 'å' kommer før 'æ' og 'ø' i ASCII-tabellen, men etter 'æ' og 'ø' i det norske alfabetet.

NB! dette kan være nyttig å merke seg i oppgave b)

<br>

## OPPGAVER

Alle deloppgaver skal besvares her: *[Sammenligning av strenger.py](Sammenligning%20av%20strenger.py)* !

## a)
Du skal nå lage et program som sammenligner to matvarer, og sjekker om de er like. Det skal ikke spille noen rolle om matvarenavnene som sendes inn har store eller små bokstaver, dvs. at `"druer"` og `"DrUer"` skal regnes som like matvarer. Det trengs ikke å ta hensyn til entall og flertall, dvs. at `"druer"` og `"drue"` regnes som to forskjellige matvarer.

**Eksempel på flere kjøring:**

___
*Sammenligner druer og DrUer <br>
Det er samme matvare*
___


*Sammenligner druer og drue <br>
Dette er to forskjellige matvarer*

___
*Sammenligner TomAT og ToMat <br>
Det er samme matvare*
___
*Sammenligner tomat og potet* <br>
*Dette er to forskjellige matvarer*
___

<br>

## b)

I denne oppgaven skal du lage et program som tar inn to navn og sorterer dem i alfabetisk rekkefølge. Du trenger ikke ta hensyn til små og store bokstaver i denne oppgaven.

**Eksempel på kjøring:**
___
*Første navn:* Ole <br>
*Andre navn:* Ola <br>
*Under følger navnene i alfabetisk rekkefølge: <br>
Ola <br>
Ole*
___
*Første navn:* Bob Bernt <br>
*Andre navn:* Bob Arne <br>
*Under følger navnene i alfabetisk rekkefølge: <br>
Bob Arne <br>
Bob Bernt*
___

<br>

**Frivillig** <br>
Endre på koden din slik at det blir tatt hensyn til æ, ø og å. Her må du nok benytte deg av stoff som ikke er gjennomgått enda.

<br>

## c)
Hva vil kodesnuttene under skrive ut til skjerm?

```python
#Kodesnutt 1:
if 'k' < 'b':
    print('k er mindre enn b')
else:
    print('k er større enn b')
  
  
#Kodesnutt 2:
ny = "New York"
la = "Los Angeles"
  
if ny < la:
    print(ny)
    print(la)
else:
    print(la)
    print(ny)
  
  
#Kodesnutt 3:
d1 = "DRuEr"
d2 = "drUer"
if d1.lower() < d2.lower():
    print(d1)
    print(d2)
else:
    print(d2.lower())


#Kodesnutt 4:
str1 = "æ"
str2 = "å"

if str1>str2:
    print(False)
else:
    print(True)
```