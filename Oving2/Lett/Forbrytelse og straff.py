
# Underveis i oppgaveløsningen kan det være greit å kommentere 
# ut de variablene som er irrelevante for oppgaven du jobber med. 

# leser inn data

prom = float(input("Hvor stor var promillen? "))
motor = input("Var farkosten en motorvogn? (j/n) ")
f = input("Var personen fører av vognen? (j/n) ")
leds = input("Var personen ledsager ved øvekjøring? (j/n) ")
n = input("Var det nødrett? (j/n) ")
nekt = input("Nektet å samarbeide ved legetest? (j/n) ")
tidl = input("Tidligere dømt for promillekjøring? (j/n) ")
if tidl == "j":
    aar = int(input("Antall år siden siste domfellelse: "))
else:
    aar = 999


print("Oppgave a og b)")
#TODO løs oppgave a og b her:
# vurderer straffbarhet
if prom < 0.2 and motor == "j" and f == "j" or leds == "j" and n == "n":
   print("Det var straffbar promillekjøring.")
else:
    print("Ikke straffbart, ingen bot.")


print("Oppgave c")
#TODO løs oppgave c her:
if prom < 0.2:
    print("Ikke straffbart, ingen bot.")
elif prom > 0.5:
    print("Bot: en halv brutto månedslønn, samt fengsel.")
elif prom > 0.4:
    print("Forelegg: 10000,-")
elif prom > 0.2:
    print("Forelegg: 6000,-")



print("Oppgave d")
#TODO løs oppgave d her:

# vurderer inndragning av førerkort
if tidl == "j" or nekt == "j":
    if aar < 5:
        print("Førerkort inndras for alltid.")
    else:
        print("Førerkort inndras minst 2 år.")
elif prom > 1.2 or nekt == "j":
    print("Førerkort inndras minst 2 år.")
elif prom > 0.8:
    print("Førerkort inndras 20-22 mnd.")
elif prom > 0.5:
    print("Førerkort inndras vanligvis 18 mnd.")
elif prom > 0.2:
    print("Førerkort inndras inntil 1 år.")
else:
    print("Ingen inndragning av førerkort.")

