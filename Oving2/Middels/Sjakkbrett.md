### **[Intro øving 2](../Intro_Øving2.md)**

<br>

# Sjakkbrett

**Læringsmål:**

* Betingelser
* Logiske uttrykk
* Nøstede if-setninger

**Starting Out with Python:**

* Kap. 2.7
* Kap. 3.2-3.3
* Kap. 3.5
 
Posisjoner på et sjakkbrett kan identifiseres med en bokstav og et tall. Bokstaven identifiserer kolonnen (kalt "linjen" i sjakk), og tallet identifiserer raden.


![img](Resources/Sjakkbrett.png)


## OPPGAVER
Alle deloppgaver skal besvares her: *[Sjakkbrett.py](Sjakkbrett.py)* !

<br>

## a)
**I denne oppgaven skal du lage et program som tar inn en posisjon fra brukeren, og bruker if-setninger til å finne ut hvilken farge ruten for denne posisjonen har.**

Ta utgangspunkt i de tre kodelinjene utdelt i .py-filen, som hjelper deg litt på vei. Disse viser hvordan man kan bruke indeksering av en strengvariabel (her kalt `pos`) for å finne enkelttegn.

Indeks 0 er det første tegnet i strengen, når `pos = 'a5'` vil dermed `pos[0]` være `'a'` og `pos[1]` være `'5'`.

I din kode må du

* bytte ut kodelinjen `posisjon = 'a5'` med en kodelinje som i stedet ber bruker om å taste inn posisjonen
* legge til mer kode under kodelinjen `tall = ...` for å ta en beslutning om den innskrevne posisjonen er hvit eller svart rute.


Eksempel på kjøring:

  
*Posisjon:* a5 <br>
*Svart*  
___
*Posisjon:* d3 <br>
*Hvit* 
___
*Posisjon:* f6 <br>
*Svart*

<br>

**Hint** <br>
Modulo (`%`) !


<br>

## b) Valgfri ekstraoppgave

Denne trenger du ikke ha fått til for å få godkjent deloppgaven.

**Utvid programmet så det også håndterer mulig feil input fra bruker, på følgende måte**:

* Hvis brukeren skriver inn en streng med en annen lengde enn 2 tegn (enten kortere eller lenger), skal programmet svare `"Feil input. Du må skrive akkurat to tegn"` og så avslutte.
* Hvis innskrevet tekst er 2 tegn lang men ikke tilsvarer en sjakkrute, skal programmet svare som vist under. Dette skal altså skje hvis første tegn noe annet enn en bokstav A-H eller a-h, eller andre tegn er noe annet enn et tall 1-8.

Hvis lengden er to tegn, med bokstav og tall innen lovlige verdiområder, skal programmet gi samme resultat som i kjøringene i oppgave a