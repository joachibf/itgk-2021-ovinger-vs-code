### **[Intro øving 2](../Intro_Øving2.md)**

<br>

# Andregradsligning

**Læringsmål:**
- Betingelser

**Starting Out with Python:**
- Kap. 3.4

I denne oppgaven skal du lage et program som tar inn input fra brukeren om tallene i en andregradsligning, og returnerer hvorvidt ligningen har to reelle løsninger, to imaginære løsninger eller én reell dobbeltrot. I tillegg skal programmet returnere løsningene dersom de er reelle.

Den generelle formen til en andregradsligning er 


ax<sup>2</sup> + bx + c = 0


For å finne ut hvor mange løsninger og hvilket løsningsområde en andregradsligning har, kan man bruke *diskriminanten*


d = b<sup>2</sup> - 4ac

og tabellen:

Tilfelle|Løsningsområde|Antall røtter
---|---|---
d < 0|Imaginær / Kompleks|2
d > 0|Reell|2
d = 0|Reell (Dobbeltrot)	|1

<br>

## OPPGAVER
Alle deloppgaver skal løses her: *[Andregradsligning.py](Andregradsligning.py)* !

## a)
***Lag et program som ber om de tre verdiene a, b og c, regner ut d, og forteller brukeren om ligningen har to imaginære løsninger, to reelle løsninger, eller én reell dobbeltrot.***

Sjekk for alle de tre mulige utfallene:

- To imaginære løsninger
  - f.eks.: a = 2, b = 4, c = 9
- To reelle løsninger
  - f.eks.: a = 2, b = -5, c = 0
- Én reell dobbeltrot
  - f.eks.: a = 2, b = 4, c = 2


<br>

## b)

***Utvid nå programmet i a) slik at bruker får vite løsningen(e) dersom de(n) er reell(e)***

Andregradsformelen for å finne løsningen(e) er:


$x=\frac{-b±\sqrt{d}}{2a}$

**Eksempel på kjøring av kode:**


Eksempel 1: imaginære løsninger <br> 
*a:* 2  
*b:* 4  
*c:* 9  
*Andregradsligningen 2.00*x^2 + 4.00*x + 9.00 har to imaginære løsninger*
___
Eksempel 2: reelle løsninger <br>
*a:* 2 <br>
*b:* -5 <br>
*c:* 0 <br>
*Andregradsligningen 2.00*x^2 -5.00*x + 0.00 har de to reelle løsningene 2.50 og 0.00*
___
Eksempel 3: reell dobbeltrot <br>
*a:* 2 <br>
*b:* 4 <br>
*c:* 2 <br>
*Andregradsligningen 2.00*x^2 + 4.00*x + 2.00 har en reell dobbeltrot -1.00*


<br>

## c)
Andregradsformelen kan i enkelte tilfeller gi feil svar. Et slikt tilfelle er når man bruker den på likningen  x<sup>2</sup>+9<sup>12</sup>x−3=0. Om man ønsker å finne røttene til denne likningen, gir standardformelen følgende resultat:


*Andregradsligningen 1.00*x^2 + 282429536481.00*x -3.00 <br> har de to reelle løsningene 0.000e+00 og -2.824e+11*


Her er bare den siste av røttene korrekt. Den første roten er ukorrekt blitt null pga. avrundingsfeil som følge av tap av presisjon i beregningen, korrekt svar ville ha vært:


*Andregradsligningen 1.00*x^2 + 282429536481.00*x -3.00 <br> har de to reelle løsningene 1.062e-11 og -2.824e+11*

Hva kommer denne feilen av? <br>
 ***Gjør om på programmet slik at det tar hensyn til slike tilfeller.***

