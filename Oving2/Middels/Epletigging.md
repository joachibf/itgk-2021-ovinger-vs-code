### **[Intro øving 2](../Intro_Øving2.md)**

<br>

# Datamaskinen som tigget epler - Debugging

**Læringsmål:**

* Feilretting
* Betingelser

**Starting Out with Python:**

* Kap. 3: Decision Structures and Boolean Logic

I denne oppgaven får du utdelt en kodesnutt som inneholder feil. Det er både konkrete feil i koden (python-syntaks) og logiske feil (brukeren kan gi input som får programmet til å gi betydningsløse svar). 

Din oppgave er å rette opp i syntaks-feilene, og håndtere de logiske feilene på en fornuftig måte. 

<br>

## OPPGAVER
Alle oppgaver skal besvares her: *[Epletigging.py](Epletigging.py)* !

## a)
Programmet spør brukeren hvor mange epler han har, og hvor mange han er villig til å gi bort. Basert på disse svarene er det tre mulige utfall (Se utdelt kode!!).


**Finn syntaksfeilene og rett opp i dem slik at koden kjører som vist i eksempelteksten:**


*Dette er et program for å teste din sjenerøsitet. <br>
Hvor mange epler har du?* 0 <br>
*Æsj, det sier du bare for å slippe å gi noe!*

___
*Dette er et program for å teste din sjenerøsitet. <br>
Hvor mange epler har du?* 5 <br>
*Hvor mange kan du gi til meg?* 2 <br>
*Du beholder det meste for deg selv... <br>
Du har nå 3 epler igjen.*

___

*Dette er et program for å teste din sjenerøsitet. <br>
Hvor mange epler har du?* 7 <br>
*Hvor mange kan du gi til meg?* 5 <br>
*Takk, det var snilt! <br>
Du har nå 2 epler igjen.* 

<br>


## b)
Utskriften av siste print-setning vil bli grammatisk feil hvis brukeren har igjen 1 eple, siden det skrives «epler». Utvid programmet slik at det skriver:

*Du har nå 1 eple igjen.*

hvis det ble akkurat ett, ellers epler i flertall som det står nå.

<br>

## c)
Hvis brukeren skriver inn et større tall for hvor mange epler man kan gi bort enn hva man faktisk hadde, kommer siste linje ut med et negativt tall.

Utvid programmet ytterligere slik at hvis brukeren f.eks. har 7 epler men vil gi bort 9, avslutter programmet med:

*Du har nå 0 epler igjen. Gi meg de 2 du skylder meg neste gang vi møtes.*