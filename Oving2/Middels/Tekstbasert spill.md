### **[Intro øving 2](../Intro_Øving2.md)**

<br>

# Tekstbasert spill


**Læringsmål:**

- Betingelser

**Starting Out with Python:**

- Kap. 3.1- 3.2
- Kap. 3.4

Noe gøy vi kan gjøre med if-setninger er å lage et enkelt tekstbasert spill. Her er et eksempel fra starten av et av de mest kjente tekstbaserte spillene, Zork:


```
West of House

This is an open field west of a white house, with a boarded front door.
There is a small mailbox here.
A rubber mat saying 'Welcome to Zork!' lies by the door.

>open mailbox
You open the mailbox, revealing a small leaflet.

>read leaflet
(first taking the small leaflet)
          WELCOME TO ZORK

ZORK is a game of adventure, danger, and low cunning.  In it you will explore some of the most amazing territory ever seen by mortal man.  Hardened adventurers have run screaming from the terrors contained within!
    ...

    
Som du ser gjør man kommandoer i spillet ved å skrive inn hva man ønsker å gjøre. Da må man passe på at programmet klarer å kjenne igjen flere kommandoer en bruker kan tenke å skrive inn.
```

## OPPGAVER
Alle deloppgaver skal besvares her: *[Tekstbasert spill.py](Tekstbasert%20spill.py)* !

## a) 
Skriv en intro til spillet ditt på en eller to setninger. La så brukeren skrive inn hva den ønsker å gjøre, og lag feedback for minst tre forskjellige muligheter ved hjelp av if-setninger. (Ønsker brukeren å åpne en dør, hva skjer? Ønsker brukeren å løpe vekk, hva skjer? Se på Zork eksempelet over eller test det ut selv [her](http://zorkonline.net/) om du trenger inspirasjon.) Husk å skriv kode for minst tre forskjellige scenarioer. I denne deloppgaven trenger du ikke ta hensyn til om brukeren skrev inn noe annet enn du forventet.

Eksempel:

*Du står utenfor en dør* <br>
gå inn <br>
*Døren er låst.*
___
*Du står utenfor en dør.* <br>
bank på <br>
*Ingen svarer*
___
*Du står utenfor en dør.* <br>
lås opp <br>
*Du låser opp døren*

<br>
<br>


## b)

I visse tilfeller vil brukeren skrive inn noe annet enn vi forventer. Utivd spillet ditt slik at det tar hensyn til dette ved å si i fra dersom noe brukeren skriver ikke støttes.

Eksempel:

*Du står utenfor en dør* <br>
gå inn <br>
*Døren er låst.*
___
*Du står utenfor en dør.* <br>
bank på <br>
*Ingen svarer*
___
*Du står utenfor en dør.* <br>
Løp og gjem deg <br>
*Dette er ikke en støttet kommando*

<br>
<br>

## c)
Endre spillet så det ikke spiller noen rolle om brukeren skriver inn store eller små bokstaver i kommandoen sin.

Eksempel:

*Du står utenfor en dør* <br>
gå inn <br>
*Døren er låst.*
___
*Du står utenfor en dør* <br>
GÅ INN <br>
*Døren er låst.*

<BR>
<BR>

## d)
Ofte når vi driver med denne slags kommandoer vil det være nesten like formuleringer som betyr det samme, men siden vi nå programmerer må svaret være eksakt det samme som vi forventer. **For hvert scenario, ta hensyn til at brukeren kan formulere kommandoen på minst to måter.**

Eksempel:

*Du står utenfor en dør* <br>
gå inn <br>
*Døren er låst.*
___
*Du står utenfor en dør* <br>
Gå inn døren <br>
*Døren er låst.*

