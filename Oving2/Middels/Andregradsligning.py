from abc import abstractmethod
import sys
from math import sqrt
print("Andregradsligninger!")
#TODO løs oppgavene her:

#A
def func_a(a=None, b=None, c=None, d=None):
    if(d==None):
        a = int(input("Input A coefficient: "))
        b = int(input("Input B coefficient: "))
        c = int(input("Input C coefficient: "))
        d = b**2 - 4*a*c
    if d == 0:
        print(f"The function {a}x^2 +({b})x + ({c}) has one real double root")
    elif d < 0:
        print(f"The function {a}x^2 +({b})x + ({c}) has two imaginary/complex roots")
    else:
        print(f"The function {a}x^2 +({b})x + ({c}) has two real roots")

#B
def func_b():
    a = int(input("Input A coefficient: "))
    b = int(input("Input B coefficient: "))
    c = int(input("Input C coefficient: "))
    d = b**2 - 4*a*c
    if(d<0):
        func_a(a,b,c,d)
    elif(d>0):
        (x1,x2) = (((-b+(b**2 - 4*a*c)**0.5)/(2*a)),((-b-(b**2 - 4*a*c)**0.5)/(2*a)))
        print(f"The function {a}x^2 +({b})x + ({c}) has two real roots x1:{x1} and x2:{x2}")
    else:
        x = (-b+(b**2 - 4*a*c)**0.5)/(2*a)
        print(f"The function {a}x^2 +({b})x + ({c}) has one real double root x:{x}")

#C
def func_c():
    '''Expansion upon func_b which allows for coefficients that produce a b ≈ d scenario'''
    print("Function C is made especially for quadratic equations-\n where B is significantly larger than A and C\nSo that B ≈ sqrt(b**2 - 4ac), causing rounding errors-\n because -b+sqrt(d) gets too close to 0\n\n")
    a = int(input("Input A coefficient: "))
    b = input("Input B coefficient: ")
    if('**' in b):
        base,exp = [int(i) for i in b.split('**')]
        b = int(base**exp)
    else:
        b=int(b)
    c = int(input("Input C coefficient: "))
    b_sq = b**2
    d = b_sq - 4*a*c
    if(d<0):
        func_a(a,b,c,d)
    elif(d>0):
        if(b>0):
            x1 = ((-2*c)/(b+sqrt(d)))
            x2 = ((-b-(d)**0.5)/(2*a))
        elif(b<0):
            x1 = ((-b+(d)**0.5)/(2*a))
            x2 = ((-2*c)/(b-sqrt(d)))
        else:
            (x1,x2) = (((-b+(d)**0.5)/(2*a)),((-b-(d)**0.5)/(2*a)))
        print(f"The function {a}x^2 +({b})x + ({c}) has two real roots x1:{x1} and x2:{x2}")
    else:
        x = (-b+(d)**0.5)/(2*a)
        print(f"The function {a}x^2 +({b})x + ({c}) has one real double root x:{x}")




if __name__ == '__main__':
    if 'a' in sys.argv or 'A' in sys.argv:
        func_a()
    if 'b' in sys.argv or 'B' in sys.argv:
        func_b()
    if 'c' in sys.argv or 'C' in sys.argv:
        func_c()
    if len(sys.argv)<2:
        print(f"This script takes assignment letters as arguments.\nProper use is 'python {sys.argv[0]} a'\nor 'python {sys.argv[0]} a b c'")