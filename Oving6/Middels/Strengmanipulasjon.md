### **[Intro øving 6](../Intro_Øving6.md)**

<br>

# Strengmanipulasjon

**Læringsmål:**

* Strenger
* Funksjoner
* Lister
* Løkker

**Starting Out with Python:**

* Kap. 8.3

## OPPGAVER
Alle deloppgaver skal besvares her: *[Strengmanipulasjon.py](Strengmanipulasjon.py)* !

<br>

## a)
Lag funksjonen `find_substring_indexes` som tar inn to strenger som argumenter (str1 og str2). Funksjonen skal finne plasseringen til alle substrengene av str1 i strengen str2. Den første indeksen til hver forekomst av str1 som substreng i str2 skal samles i en liste, og denne listen skal returneres.  
Funksjonen tar ikke hensyn til store og små bokstaver, altså vil str2 = "Is this the real life? Is this just fantasy?" ha 4 substrenger av str1 = "iS"; "**Is** th**is** the real life? **Is** th**is** just fantasy?".

**Eksempel på kjøring:**
```python
str1 = "iS", str2 = "Is this the real life? Is this just fantasy?"
print(find_substring_indexes(str1,str2))
```
$-> [0, 5, 23, 28]$

```python
str1 = "oo", str2 = "Never let you go let me go. Never let me go ooo"
print(find_substring_indexes(str1,str2))
```
$-> [44, 45]$

Legg merke til at 'ooo' telles som to substrenger av 'oo'!

<br>

## b)
Nå skal du lage en funksjon som tar inn tre strenger som argumenter (str1, str2 og str3). Funksjonen skal finne alle substrenger av str1 i str2, og returnere en streng der disse substrengene i str2 er endret til str3. (Dette skal gjøres uten å bruke innebygde funksjoner.)

**Eksempel:**<br>
Hvis str1,str2 og str3 er:

`str1` = "iS"<br>`str2` = "Is this the real life? Is this just fantasy?"<br>`str3` = "cool"<br>

Da skal funksjonen returnere: <br>
"cool thcool the real life? cool thcool just fantasy?"

<br>

**Hint** <br>
Bruk funksjonen fra a).


