### **[Intro øving 6](../Intro_Øving6.md)**

<br>

# Kryptering

**Læringsmål:**

* Lister
* Tekstbehandling
* Nettverkssikkerhet 

**Starting out with python:**

* Kap. 7: Lists and Tuples
* Kap. 9.2: Sets

<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Kryptering.py](Kryptering.py)* !

<br>

James Bond er på et topphemmelig oppdrag i Langtvekkistan for MI6. Han ønsker å kunne kommunisere med oppdragsgiveren sin via internett, men myndighetene i Langtvekkistan overvåker alt som sendes over nett i landet. Derfor må Bond og MI6 bruke noe som heter [kryptering](https://www.datatilsynet.no/regelverk-og-skjema/behandle-personopplysninger/kryptering/) (datatilsynet) når de kommuniserer, slik at de vet at ingen andre kan lese meldingene de sender seg i mellom. Oppdraget ditt er nå å lage denne krypteringen og for å gjøre det bruker vi en metode kalt "[One-time pad](https://en.wikipedia.org/wiki/One-time_pad)". Den fungerer slik: 

Krypteringen foregår ved at man har en melding M og en nøkkel K, og regner ut C definert under: 

* m = bokstav i ordet M
* k = hemmelig bokstav fra ordet K
* c = m [XOR](https://en.wikipedia.org/wiki/Exclusive_or) k (den krypterte bokstaven i C)
(dvs. 1101 XOR 1011 = 0110)

For å dekryptere meldingen kan vi ved hjelp av passordet gjøre den motsatte operasjonen i og med at:

* m = c XOR k

Det er lagt ved to hjelpefunksjoner `toHex`og `toString` i .py filen. Disse kan du bruke som du ønsker.

`toHex` tar ikke inn en normal streng, men tar inn en binær streng. Koden nedenfor viser hvordan du konverterer en vanlig streng til binærstreng:

```python
string = bytes(string, encoding = 'ascii')
```

<br>

## a)
Lag en funksjon `encrypt(message, key)` som tar inn en streng `message` og en nøkkel `key` (som er like lang som message, dvs. len(key)=len(message)) og returnerer det krypterte ordet ved å bruke XOR-operasjonen. 

Du må først gjøre om strengene `message` og `key` til heksadesimal v.h.a. funksjonen `toHex(word)` definert over. 

Funksjonen skal returnere heltallet `code` som er gitt ved M XOR K.

Hint: XOR i python implementeres med `^`-operatoren.

**Eksempel på kjøring:**
```python
print(encrypt('hei','abc')) #msg='hei', key='abc'
```
$-> 591626$

<br>

## b)
Lag en funksjon `decrypt(code, key)` som tar inn en kode generert fra encrypt og nøkkelen **key**, og returnerer meldingen. 

Husk at M = C XOR K.

**Eksempel på kjøring:**
```python
print(decrypt(591626,'abc')) #key = 'abc', code = 591626
```
$-> hei$

<br>

## c)
 Lag en funksjon `main()` som tar inn en setning fra bruker, genererer en tilfeldig nøkkel, skriver ut den krypterte setningen og så den dekrypterte setningen.
 
 **Eksempel på kjøring.**

>*Hva er meldingen?* God dag, Bob Bernt <br>
*Krypto: 8976018785527472660004435694777950941489408 <br>
Melding: God dag, Bob Bernt* <br>


<br>

## d) Frivillig
Dersom samme nøkkel blir brukt til å kryptere to forskjellige ord kan man bruke resultatet og en "ordbok" til å finne tilbake til de opprinnelige ordene, og derfor også nøkkelen. Forklar hvordan dette kan gjøres.




