### **[Intro øving 6](../Intro_Øving6.md)**

<br>

# Strenger og konkatinering

**Læringsmål:**

* Strenger
* Løkker
* Funksjoner
* Lister

**Starting Out with Python:**

* Kap. 7.2
* Kap. 8.1

<br>

## INTRODUKSJON
### **Konkatinering**

Konkatinering er å tilføye en streng ved slutten av en annen streng. Dette gjøres enten vha. + eller +=. For å bruke += må variabelen på venstre side av += være en allerede eksisterende variabel. Se på eksemplene under og se om du skjønner hvordan de virker.

```python
# eksempel 1
# Bruk av +
melding = "Hei " + "der"
print(melding)

# eksempel 2
s1 = "Hei"
s2 = "der"
melding1 = s1 + " " + s2
print(melding1)

# eksempel 3
# Bruk av +=
navn = "Bob"
navn += " " + "Bernt"
print(navn)

# eksempel 4
liste = ["Geralt","of","Rivia"]
navn2 = ""
for streng in liste:
    navn2 += streng + " "
print(navn2)

# eksempel 5
# Vil utløse et unntak (exception) siden variabelen navnet ikke eksisterer fra før.
# Unntaket som utløses vil være NameError
navnet += "Bob"  
```
<br>

### **Ikke-muterbarhet**
Strenger er ikke-muterbare, som vil si at man ikke kan endre på deler av strengen uten å lage en ny streng. Konkatinering kan gi inntrykket av å endre på en streng, mens det egentlig opprettes en ny en.

```python
navn = "Bob"        # tilordner strengen "Bob" til variabelen navn
navn += " Bernt"    # tilordner en NY streng "Bob Bernt" til variabelen navn
```
Siden strenger er ikke-muterbare, betyr det at man ikke kan endre på enkelte karakterer i strengen. Altså kan man ikke skrive string[index] = "c".

```python
navn = "Bob Bernt"
navn[0] = "H"       # IKKE MULIG!! Vil få utløst et unntak (prøv selv)
```
Om man ønsker å beholde store deler av en streng, men endre på enkelte deler av den, kan man bruke slicing til å opprette en ny streng med de ønskede endringene.



<br>

<br>

## OPPGAVER
Alle deloppgaver skal løses her: *[Strenger og konkatinering.py](Strenger%20og%20konkatinering.py)* !

## a)
Skriv en funksjon som tar inn to strenger (s1 og s2), og returnerer én streng bestående av disse med et mellomrom mellom.

<br>

## b)
Skriv en funksjon som tar inn en liste av strenger, og returnerer én streng bestående av disse uten mellomrom mellom dem.

<br>

## c)
Skriv en funksjon som tar inn en liste med strenger og skriver ut den første karakteren i hver av dem.

<br>

## d) Kodeforståelse
Hva vil kodesnutten under skrive ut til skjerm?

```python
def func1(liste):
    streng = ""
    for s in liste:
        if len(s)>3:
            streng += s[3]
    return streng
 
def func2(streng):
    streng += streng
    return streng

print(func2(func1(["Klabert","Oslo","Tur","stubbe"])))
```


