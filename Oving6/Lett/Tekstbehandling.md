
### **[Intro øving 6](../Intro_Øving6.md)**

<br>

# Tekstbehandling

**Læringsmål:**

* Strenger
* Funksjoner
* Løkker
* Kodeforståelse

**Starting Out with Python:**

* Kap. **8.3**

<br>

## INTRODUKSJON
### **Testing av strenger**

Python har to nyttige operatoerer `in` og `not in`. Disse kan brukes til å sjekke om en streng er inneholdt i en annen.

La streng 1 og streng 2 være tilordnet to forskjellige strenger.
Det logiske uttrykket:
```python
streng1 in streng2
```
vil returnere True om streng1 er inneholdt i streng2, f.eks vil uttrykket
```python
"Ber" in "Bob Bernt"
```
returnere True, mens uttrykket
```python
"obo" in "Bob Bernt"
```
returnerer False.
Tilsvarende vil det logiske uttrykket
```python
streng1 not in streng2
```
returnere True om streng1 IKKE er inneholdt i streng2, altså det motsatte av det streng1 in streng2 ville returnert.

Strenger i Python har flere metoder som kan brukes til å teste en streng for bestemte karaktertrekk. Tabellen under viser enkelte av disse:

Method|Description
--- | --- 
isalnum()|Returns true if the string contains only alphabetic letters or digits and is at least one character in length. Returns false otherwise.
isalpha()|Returns true if the string contains only alphabetic letters, and is at least one character in length. Returns false otherwise.
isdigit() | Returns true if the string contains only numeric digits and is at least one character in length. Returns false otherwise.
islower()| Returns true if all of the alphabetic letters in the string are lowercase, and the string contains at least one alphabetic letter. Returns false otherwise.
isupper() | Returns true if all of the alphabetic letters in the string are uppercase, and the string contains at least one alphabetic letter. Returns false otherwise.
isspace() | Returns true if the string contains only whitespace characters, and is at least one character in length. Returns false otherwise. (Whitespace characters are spaces, newlines(\n), and tabs(\t)).
 
<br>
Generelt format for bruk av disse metodene er: streng.metode(argument)

F.eks. vil `streng.isupper()` returnere *True* om alle bokstavene i streng er blokkbokstaver og lengden til strengen er større eller lik 1.

Metodene i tabellen over er boolske metoder som vil returnere enten *True* eller *False*.

<br>

### **Modifisering av strenger**

Strengemodifiserings-metoder (se tabellen nedenfor) er metoder som returnerer en modifisert kopi av strengen metoden kalles på. På denne måten kan man "simulere" at strenger endres, enda de ikke egentlig vil dette (strenger er jo ikke-muterbare).

<br>

Method | Description
---|---
lower() | Returns a copy of the string with all alphabetic letters converted to lowercase. Any character that is already lowercase, or is not an alphabetic letter, is unchanged. 
upper() | Returns a copy of the string with all alphabetic letters converted to uppercase. Any character that is already uppercase, or is not an alphabetic letter, is unchanged. 
lstrip() | Returns a copy of the string witg all leading whitespace characters removed. Leading whitespace characters are spaces, newlines(\n), and tabs(\t) that appear at the beginning of the string.
lstrip(*char*) | The *char* argument is a string containing a character. Returns a copy of the string with all instances of *char* that appear at the beginning of the string removed. 
rstrip() | Returns a copy of the string witg all trailing whitespace characters removed. Trailing whitespace characters are spaces, newlines(\n), and tabs(\t) that appear at the end of the string.
rstrip(*char*) | The *char* argument is a string containing a character. Returns a copy of the string with all instances of *char* that appear at the end of the string removed. 
strip() | Returns a copy of the string with all leading and trailing whitespace characters removed.
strip(*char*) | Returns a copy of the string with all instances of *char* that appear at beginning and the end of the string removed. 

<br>

### **Søking i strenger og erstatting av karakterer**

Det finnes også flere metoder som kan søke og finne substrenger i en streng, om de skulle være til stede. I tillegg er det mulig å bruke metoden `streng.replace(old,new)` til å få en kopi av streng der alle instanser av *old* er erstattet med instanser av *new*.

<br>

### **Repetisjons-operatoren og splitting av strenger**
Som du alt har lært, kan man duplisere en liste vha. repetisjons-operatoren *. Det samme gjelder for strenger. Det generelle formatet er:

*`streng * n`*

Dette vil gi en streng bestående av n repeterte kopier av strengen *streng*.

Python har en metode kalt *split* som returnerer en liste med ordene i strengen metoden blir kalt på. Dvs. at `s.split()` returnerer en liste bestående av ordene i strengen s.

*split*-metoden kan ta inn en streng som argument. Om *split*-metoden ikke får inn noe argument vil den bruke mellomrom som skilletegn. Om den derimot får inn et argument, f.eks. '-', vil den splitte på disse, og ikke mellomrom.

```python
#Eks 1
s = "Lilac and gooseberries"
liste = s.split()           #Vil bruker standard-skilletegnet som er mellomrom
print(liste)
  
#Eks 2
s = "ISBN 978-0-765-32635-5"
liste = s.split('-')        #Vil bruke '-' som skilletegn
print(liste)
  
#Eks 3
s = "Numuhukumakiaki'aialunamor"
liste = s.split('u')        #Vil bruke 'u' som skilletegn
print(liste)

#Eks 4
s = "98765439876542987698762498721598652976298763"
liste = s.split('987')      #Vil bruke '987' som skilletegn
print(liste)
```


<br>
<br>

## OPPGAVER
Alle deloppgaver skal løses her: *[Tekstbehandling.py](Tekstbehandling.py)* !

## a)
Skriv en funksjon som tar inn en streng som argument, og endrer bokstavene i den til blokkbokstaver. I tillegg skal funksjonen fjerne eventuell whitespace på start og slutt av strengen. Den resulterende strengen skal så returneres.

Hvis funksjonen kjøres med input `streng = " \n  Far over the Misty Mountains \t "` så skal output være `FAR OVER THE MISTY MOUNTAINS`.

<br>

## b)
Lag en funksjon som tar inn en streng og en karakter som argumenter. Funksjonen skal splitte strengen med hensyn på denne karakteren, og returnere listen man får av denne splittingen. Se eksempel over om splitting hvis du er usikker. 

<br>

## c) Kodeforståelse
Hva vil følgende kodesnutt skrive ut til skjerm?
```python
s1 = "eat"
s2 = "I want to be like a caterpillar. Eat a lot. Sleep for a while. Wake up beautiful."

def func(s1, s2):
    s = "My bed is a magical place where I suddenly remember everything I forgot to do."
    if s1 in s2.lower():
        s = "The more you weigh, the harder you are to kidnap. Stay safe. Eat cake."
    print(s)
    
    
func(s1,s2)
```

<br>

<br>

## d)
Skriv en funksjon som, vha. løkker og repetisjons-operatoren, skriver ut følgende til skjerm:
    
>Z <br>
ZZ <br>
ZZZ <br>
ZZZZ <br>
ZZZZZ<br>
ZZZZZZ<br>
ZZZZZZZ<br>
ZZZZZZZZ<br>
ZZZZZZZ<br>
ZZZZZZ<br>
ZZZZZ<br>
ZZZZ<br>
ZZZ<br>
ZZ<br>
Z<br>

<br>




