### **[Intro øving 6](../Intro_Øving6.md)**

<br>

# Aksessering av karakterer i en streng

**Læringsmål:**

* Strenger
* Funksjoner

**Starting Out with Python:**

* Kap. 8.1


<br>

## INTRODUKSJON

### **Å iterere over strenger**

Man kan lett iterere over en streng ved hjelp av en for-løkke. Den generelle formen for dette er:

```python
for character in string:
    # do something
```
For hver iterasjon vil character-variabelen referere til en karakter i string, og den vil starte med å referere til den første karakteren. Vi sier at løkken itererer over strengen.
 <br>

### **Indeks**

Å iterere over en streng med for-løkke er en måte å aksessere hver enkelt karakter i en streng. En annen måte for å aksessere karakterene i en streng på er å bruke indeks. Alle karakterene i en streng har hver sin indeks knyttet til deres plassering i strengen:

Eks:
```python
my_string = "Roser er røde"
my_string[0] #ville returnert "R"
my_string[3] #ville returnert "e"
my_string[5] #Ville returnert " "
```

Som man kan se i eksemplet over, starter indekseringen med 0, akkurat som for lister. For å få tak i en av karakterene og tilordne den til en variabel, skriver man
ch = my_string[3],
dette vil tilordne ch til 'e'.

Hver karakter i strengen kan aksesseres vha. to tall: et positivt og et negativt. Om man skal ha tak i siste karakter i strengen kan man enten bruke 12 eller -1 som indeks, dvs. både 
'ch = my_string [12]' og 'ch = my_string[-1]' vil tilordne ch til 'd'. Tilsvarende vil 1 og -12 begge være indekser for 'o' i my_string.

<br>


## OPPGAVER
Alle deloppgaver skal løses her: *[Aksessering av karakter.py](Aksessering%20av%20karakterer.py)* !

<br>

## a)
Skriv en funksjon som tar inn en streng, itererer over denne, og skriver ut hver enkelt karakter til skjerm.

**Eksempel på kjøring**:


>Argumentet er `Nilfgaard` <br>
N<br>
i<br>
l<br>
f<br>
g<br>
a<br>
a<br>
r<br>
d<br>


<br>
<br>

## b)

Skriv en funksjon `third_letter(string)` som tar inn en streng og returnerer den tredje bokstaven i strengen. Om lengden til strengen ikke er stor nok, skal funksjonen returnere 'q'. Det følger med en testkode for denne oppgaven.

<br>

## c)
Skriv en funksjon som tar inn en streng og returnerer største gyldige indeks, dvs. den største indeksen som ikke gir IndexError. Hvis du har implementert funksjonen rett, skal funksjonen returnere `13` hvis argumentet er `The White Wolf`.



