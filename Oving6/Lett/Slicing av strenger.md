### **[Intro øving 6](../Intro_Øving6.md)**

<br>

# Slicing av strenger

**Læringsmål:**

* Strenger
* Funksjoner
* Lister

**Starting Out with Python:**

* Kap. 8.2

<br>

## INTRODUKSJON
### **Slicing**
Slicing kan brukes til å velge ut en del av en streng. Formatet for dette er:  
string[start : slutt : steg]

* Den nye strengen vil starte på karakteren start-indeksen tilsvarer, fortsette med karakteren indikert av indeksen (start+steg), så ta neste karakter indikert av indeksen (start+steg+steg), osv., helt til, men ikke med, karakteren end-indeksen tilsvarer. (Se eksempeler på kjøring)
* Om start ikke er spesifisert, antas indeks 0
' Om slutt ikke er spesifisert, antas indeks len(string)
* Om steg ikke er spesifisert antas steg = 1


<br>

<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Slicing av strenger.py](Slicing%20av%20strenger.py)* !

## a)
Skriv en funksjon som tar inn en streng som argument, og returnerer en ny streng bestående av hver fjerde karakter i argumentet.

<br>

## b)
Skriv en funksjon som tar inn en liste med strenger, itererer gjennom denne, og returnerer en ny streng bestående av de to siste karakterene i strengene i listen.

<br>

## c)
Under følger tre kodesnutter, der to inneholder en feil hver, og den siste er rett. Hvilken kode er korrekt? Finn feilen i de to andre. Skriv svaret ditt som tekst i .py filen.

```python
#Kodesnutt 1
streng = "I Want Cake"
streng[7:] = "Cupcake"
print(streng)
 
#Kodesnutt 2
streng = "I Want Cake"
streng = streng[-4:100:]
print(streng)
 
#Kodesnutt 3
streng = "I Want Cake"
streng = streng[]
print(streng)
```