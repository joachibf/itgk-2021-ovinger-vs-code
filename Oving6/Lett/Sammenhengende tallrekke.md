### **[Intro øving 6](../Intro_Øving6.md)**

<br>

# Sammenhengende tallrekke

**Læringsmål:**

* Lister

**Starting Out with Python:**

* Kap. 7: Lists and Tuples

I denne oppgaven skal du sammenligne lister med tall. 

## OPPGAVER
Alle deloppgaver skal besvares her: *[Sammenhengende tallrekke.py](Sammenhengende%20tallrekke.py)* ! <br>
Det følger med en main() metode i .py filen, hvis du har gjort alle deloppgaver riktig skal denne skrive ut:
> *[5, 6, 7, 4, 7, 3, 2, 4, 3, 2*] (Merk at denne vil variere!) <br>
*[1, 3] <br>
[1] <br>
(3, 4)*<br>

## a)
Lag en funksjon `randList(size, lower_bound, upper_bpund)` som tar inn tre heltall, `size`, `lower_bound` og `upper_bound`. Denne skal returnere en liste med tilfeldige tall i intervallet mellom `lower_bound` og `upper_bound`. Hvor mange tall listen skal inneholde er angitt av `size`.

<br>

## b)
Skriv en funksjon `compareLists(list1, list2)` som sammenligner 2 lister og returnerer en liste med alle tall som var med i begge listene. Kopier skal ikke taes med. Ikke bruk innebygde funksjoner.

<br>

## c)
Skriv en ny funksjon `multiCompList(lists)` som kan sammenligne et vilkårlig antall lister.  
Hint: `lists` er en liste av lister. Ikke bruk innebygde funksjoner, men ta gjerne i bruk funksjoner du har laget tidligere i oppgaven.


<br>

## d)
Skriv en funksjon `longestEven(list)` som finner lengste sammenhengende rekke med partall i en liste. Metoden skal returnere indeksen til det første partallet i rekken og antallet partall i rekken.
