### **[Intro øving 6](../Intro_Øving6.md)**

<br>

# Ideel gasslov

**Læringsmål:**

* Lister
* Strenger
* Løkker
* Funksjoner

**Starting Out with Python:**

* Kap. 7.8: Two-Dimensional Lists
 

<br>

## INTRODUKSJON

I gasser kan man ikke endre på trykk (p), temperatur (T) og volum (V) uavhengig av hverandre. I ideelle gasser kan man uttrykke sammenhengen mellom disse størrelsene ved uttrykket

**$pV=nRT$**

Her er p trykk i Pa, V er volum i $m^{3}$, n er antall mol, R er 8.31452 J/Kmol, og T er absolutt temperatur i K.

I denne oppgaven skal vi lage et program som regner ut trykket (p) ved forskjellige volum og temperaturer, og setter opp dette i en fin tabell.


<br>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Ideel gasslov.py](Ideel%20gasslov.py)* !
## a)
Lag funksjonen `p_ig(t, v, n, rgas=8.31452)` som tar inn argumentene t (temperatur), v (volum), n (antall mol) og konstanten rgas (R). Funksjonen skal returnere trykket (p).

**Eksempel på kjøring:**

```python
print(p_ig(373.15,2.4,100))
```
$-> 129273.46408333334$
```python
print(p_ig(293,24*10**(-3),1))
```
$-> 101506.43166666667$


Legg merke til at man IKKE trenger å sende inn en konstant for rgas (men man kan om man ønsker),
siden denne blir satt til 8.31452 om det ikke blir gitt et argument

<br>

## b)
Lag funksjonen `p_ig_pptable` som tar inn en liste med forskjellige volum, en liste med temperaturer, og en konstant for antall mol. Funksjonen skal returnere en tabell (2D-liste) der volumet inngår i den første kolonnen og trykkene i de neste kolonnene.

**Eksempel på kjøring**
```python
nv = 10     # number of volumes (rows)
nt = 3      # number of temperatures (columns)
n = 10      # [mol]
t = [100 + float(j)*200 for j in range(nt)]     # [K]
v = [10**(-float(i)/nv) for i in range(1, nv+1)]

table = p_ig_pptable(v,t,n)
print(table)  
```
Skal gi utskriften:
```python
[[0.7943282347242815, 10467.36051487084, 31402.08154461252, 52336.802574354195], [0.6309573444801932, 13177.626146581779, 39532.878439745335, 65888.13073290889], [0.5011872336272722, 16589.648423055052, 49768.94526916515, 82948.24211527525], [0.3981071705534972, 20885.12997251504, 62655.389917545115, 104425.64986257518], [0.31622776601683794, 26292.820851023193, 78878.46255306958, 131464.10425511596], [0.251188643150958, 33100.70031710464, 99302.10095131393, 165503.5015855232], [0.19952623149688797, 41671.31277738628, 125013.93833215884, 208356.56388693137], [0.15848931924611134, 52461.074598274565, 157383.22379482372, 262305.37299137283], [0.12589254117941673, 66044.57994179733, 198133.739825392, 330222.89970898663], [0.1, 83145.2, 249435.6, 415725.99999999994]]
```


<br>

## c)
Utskriften fra oppgave b) er ikke særlig fin. Du skal derfor nå skrive ferdig koden i Kodesnutt, slik at det blir skrevet ut en fin tabell lignende den i eksempelet nedenfor.

**Eksempel på kjøring:**
```python
| Volum (m^3)         | Temp. = 100.0K      | Temp. = 300.0K      | Temp. = 500.0K      |
-----------------------------------------------------------------------------------------
| 0.7943282347242815  | 10467.36051487084   | 31402.08154461252   | 52336.802574354195  |

| 0.6309573444801932  | 13177.626146581779  | 39532.878439745335  | 65888.13073290889   |

| 0.5011872336272722  | 16589.648423055052  | 49768.94526916515   | 82948.24211527525   |

| 0.3981071705534972  | 20885.12997251504   | 62655.389917545115  | 104425.64986257518  |
 
| 0.31622776601683794 | 26292.820851023193  | 78878.46255306958   | 131464.10425511596  |
 
| 0.251188643150958   | 33100.70031710464   | 99302.10095131393   | 165503.5015855232   |
 
| 0.19952623149688797 | 41671.31277738628   | 125013.93833215884  | 208356.56388693137  |
 
| 0.15848931924611134 | 52461.074598274565  | 157383.22379482372  | 262305.37299137283  |
 
| 0.12589254117941673 | 66044.57994179733   | 198133.739825392    | 330222.89970898663  |
 
| 0.1                 | 83145.2             | 249435.6            | 415725.99999999994  |
```


