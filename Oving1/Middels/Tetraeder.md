### **[Intro øving 1](../Intro_Øving1.md)**
<br>

# Tetraeder 

**Læringsmål:**

* Input/output
* Formatere utskreven tekst
* Bruke innebygde funksjoner

**Starting Out with Python:**

* Kap. 2.8

<BR>

## OPPGAVER
Alle deloppgaver skal besvares her: *[Tetraeder.py](Tetraeder.py)* !

<BR>


**Introduksjon**

![img](Resources/Tetrahedron.jpg)

<br><br>I denne oppgaven skal du finne overflateareal og volum til regulære tetraedere (også kjent som trekantede pyramider). Et regulært tetraeder er et geometrisk objekt bestående av fire likesidede trekanter.

* **Overflatearealet til et tetraeder er A=$\sqrt{3}a^{2}$**  
* **Volumet til et tetraeder er V=$\frac{\sqrt{2}a^{3}}{12}$  hvor a=$\frac{3}{\sqrt{6}}h$** 

## a) 

Lag et program som beregner og skriver ut på skjerm overflatearealet til et tetraeder. Test at programmet skriver ut 23.383 som resultat når høyden, h, er 3 (det gjør ikke noe om svaret får flere siffer).

Eksempel på kjøring:
  
```python
Et tetraeder med høyde 3 har areal 23.383
```

#### **Hint**
Kvadratrot kan regnes ut enten ved å opphøye et tall i 1/2, f.eks. `x ** 0.5`, eller ved `import math` og `math.sqrt` for å regne ut røttene. Det kan være lurt å lagre verdiene i variabler.


## b)
Utvid programmet slik at det også skriver ut volumet til et tetraeder. Test at programmet skriver 5.846 når høyden (h) er 3 (det gjør ikke noe om svaret får flere siffer).

Eksempel på kjøring:

  
```python
Et tetraeder med høyde 3 har volum 5.846
```

## c)
Utvid programmet så brukeren blir bedt om å skrive inn verdien på høyden (h) fra tastaturet, og får ut volum og areal. Eksempel på kjøring vist i boksen under. 

Eksempel på kjøring:

  
```python
Skriv inn en høyde: 3
Et tetraheder med høyde 3.0 har volum 5.85 og areal 23.38
```