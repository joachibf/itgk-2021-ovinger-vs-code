### **[Intro øving 1](../Intro_Øving1.md)**
<br>

# Kalkulasjoner

**Læringsmål**:

* Basisferdigheter, sekvensiell programmering

* Utføre enkle kalkulasjoner

**Starting Out with Python:**

* Kap. 2.7

I denne oppgaven skal du lære hvordan du skriver matematiske uttrykk for å gjøre utregninger i Python.

<Br>

## INTRODUKSJON

### **Vanlige operatorer, parenteser, presedens**

Det er mange likheter mellom Python og vanlig matematisk skrivemåte av aritmetiske uttrykk, men også noen forskjeller.

Tabellen under oppsummerer det mest grunnleggende:
    
Matematikk | Python | Merknad
--- | --- | ---
a + b | a + b | Det er vanlig å sette et mellomrom på hver side av + men ikke påkrevd. <br> Kunne også ha skrevet a+b. Samme gjelder for andre regneoperatorer. <br> Smak og behag, men litt luft gjør ofte uttrykk lettere å lese.
a - b | a - b | Bruk det vanlige bindestrek-tegnet for minus
a · b |a * b| Bruk stjerntegn (asterisk) for multiplikasjon
ab|<span style="color:red">**NEI**</span>| I Python må gangetegn **alltid** skrives eksplisitt, kan ikke utelates
a : b|a / b|Vanlig skråstrek brukes for divisjon, **ikke** kolon eller horsintal brøkstrek.
a<sup>b</sup>|a ** b| Dobbel stjerne for potens. De to stjernene må stå kloss inntil hverandre.
[(a + b) * c - d]|((a + b) * c - d)|I matematisk notasjon brukes av og til ulike parentessymboler () [] {} <br> hvis det er uttrykk med flere nivåer av parenteser nøstet inn i hverandre. <br> I Python må vanlig parentes () brukes for **alle** nivåer. [] og {} har en annen betydning.

**Presedens** mellom operatorer fungerer som i matematikken.

* Multiplikasjon og divisjon har høyere presedens enn addisjon og subtraksjon.
* Potens har høyere presedens enn multiplikasjon og divisjon.
* Parenteser kan brukes for å få en annen rekkefølge på regneoperasjonene:
* Hvis du skal "oversette" et matematisk uttrykk med parenteser til Python, bruk parenteser på samme sted også i Python-koden.

<Br>

### **Heltallsdivisjon og Modulo**
I tillegg til vanlig divisjon / har Python også heltallsdivisjon som skrives // og modulo som skrives med operatoren %.

Heltallsdivisjon og modulo minner om måten du lærte divisjon på barneskolen før du hadde lært desimaltall, altså med hele tall og rest.

Tabellen under illustrerer hvordan disse operatorene virker:

**Utrykk i Python**|**Resultat**|**Forklaring**
---|---|---
17 / 5	|3.4	|Vanlig divisjon
17 // 5|	3	|Heltallsdivisjon, gir hvor mange hele ganger nevneren 5 går opp i telleren 17
17 % 5	|2|	Modulo, gir resten av 17 // 5, dvs. de 2 som blir til over
7.75 / 2.5	|3.1|	Vanlig divisjon
7.75 // 2.5	|3.0|   Heltallsdivisjon, gir hvor mange hele ganger nevneren 2.5 går opp i 7.75.<br> Her blir svaret et flyttall (3.0) heller enn heltallet 3, fordi teller og nevner er flyttall.
7.75 % 2.5	|0.25|	Modulo, Resten av 7.75//2.5 er 0.25 fordi 2.5 * 3.0 er 7.5

Heltallsdivisjon og modulo har en rekke nyttige bruksområder i programmering.

Ett eksempel er regning med enheter som aggregeres på andre måter enn det typiske 10, 100, 1000, slik som 60 sekund per minutt, 60 minutt per time, 24 timer per døgn, 7 døgn per uke.

**Eksempel :**
```python
print(215, "sekunder blir", 215 // 60, "minutter og", 215 % 60, "sekunder.")

print(53, "dager blir", 53 // 7, "uker og", 53 % 7, "dager")
```
<Br>

### **Innebygde funksjoner og konstanter**
Python har en rekke innebygde funksjoner. Vi kan skille mellom

* funksjoner i **standardbiblioteket**. Disse er alltid tilgjengelige og kan dermed brukes direkte.
* funksjoner i **andre biblioteker**. For å kunne bruke slike funksjoner må vi importere det aktuelle biblioteket i starten av programmet.

En fullstendig liste over funksjoner i standardbiblioteket fins [her](https://docs.python.org/3/library/functions.html).

Noen standardfunksjoner som inngår i ulike deloppgaver på Øving 1 er print(), input(), str(), int(), float().

Spesielt relatert til matematikk fins dessuten **abs()** for absoluttverdi og **round()** for avrunding i standardbiblioteket.

**Eksempel :**

```python
abs(-3)        #Absolutverdien til -3, returnerer 3
round(4.75)    #Returnerer 5, avrunder til helt tall
round(4.75,1)  #Returnerer 4.8, avrunder til 1 desimal.

```

Noen andre biblioteker som vi bruker i dette emnet er:

* **math** som inneholder en rekke matematiske funksjoner, f.eks. sin(), cos(), tan(), sqrt(), log(), gcd(), factorial(), samt noen vanlige matematiske konstanter som pi og e.
* **random**. som inneholder funksjoner for å generere tilfeldige tall, f.eks. random() som gir et tilfeldig tall mellom 0 og 1, og randint(a, b) for tilfeldige heltall a <= x <= b.
* **turtle**, som inneholder funksjoner for å tegne enkle grafiske figurer på skjermen, f.eks. circle() for å tegne en sirkel.
 
Disse bibliotekene må i motsetning til standarbiblioteket importeres. Dette gjøres ved å skrive "import *navnet på biblioteket som skal importeres*"

**Eksempel :**
```python
import math

math.sqrt(16)  # Returnerer kvadratroten av 16
math.sin(2)    # Returnerer sinus til 2
math.pi        # Konstanten pi ≈ 3.141592653589793
math.e         # Konstanten e ≈ 2.718281828459045
math.gcd(18,12) # Returnerer den største felles faktoren for 18 og 12. 

```

<Br>

## OPPGAVER

Alle deloppgaver skal besvares her: *[Kalkulasjoner.py](Kalkulasjoner.py)* ! Her er det skrevet halvferdige koder, og deres oppgave er å erstatte "**None**" med korrekt python-kode. 

## a) Korrekt programmering av aritmetiske utrykk
**None** skal erstattes med et aritmetisk uttrykk, ikke med svaret på regnestykket.

Hvis oppgaven er løst korrekt skrives dette til terminal:
```python
5:2-4 = -1.5
5· 12+6-1 = 65
3(5+2) = 21
4[(5+3):2 +7] = 44.0
(-4)^(-3)+5·(3-7:2) = -2.515625
```

## b) Bruk av heltallsdivisjon og modulo
**None** skal erstattes med enten // (heltallsdivisjon) eller % (modulo), slik at påstandene stemmer. 

Hvis oppgaven er løst korrekt skrives dette til terminal:
```python  
403 sekund blir 6 minutt og 43 sekund.  
67 dager blir 9 uker og 4 dager.  
100 timer blir 4 døgn og 4 timer.
```


## c) Bruk av innebygde funksjoner og konstanter
Velg en passende import-setning som du skriver inn øvert i koden.
Estratt så **None** med innebygde funksjoner og konstanter (både fra standar biblioteket og importerte biblioteker) slik at uttrykkene blir korrekte.

Hvis oppgaven er løst korrekt skrives dette til skjerm:
```python
|-8|, dvs. absoluttverdien til -8, er 8 
2.544 avrundet til helt tall er 3 
Funksjonen int() derimot bare kutter vekk desimalene: 2 
2.544 avrundet til to desimaler er 2.54 
Kvadratroten til 10 er 3.1622776601683795 
En sirkel med radius 7 har omkrets 43.982297150257104 
En sirkel med radius 7 har areal 153.93804002589985
```




