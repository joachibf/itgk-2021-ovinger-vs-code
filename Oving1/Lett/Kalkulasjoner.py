print("Oppgave a)")
#TODO løs oppgave a her:
#1)
print('5:2-4 =', None)
#2)
print('5·12+6-1 =', None)
#3)
print('3(5+2) =', None)
#4)
print('4[(5+3):2 +7] =', None)
#5)
print('(−4)^(-3)+5·(3−7:2) =', None)



print("Oppgave b)")
#TODO løs oppgave b her:

print(403, "sekund blir", None, "minutt og", None, "sekund.")
print(67, "dager blir", None, "uker og", None, "dager.")
print(100, "timer blir", None, "døgn og", None, "timer.")



print("Oppgave c)")
#TODO løs oppgave c her:

#Import-setningen plasseres her

print("|-8|, dvs. absoluttverdien til -8, er", None)
print(2.544, "avrundet til helt tall er", None)
print("Funksjonen int() derimot bare kutter vekk desimalene:", int(2.544) )
print(2.544, "avrundet til to desimaler er", None)
print("Kvadratroten til", 10, "er", None)
print("En sirkel med radius 7 har omkrets", None)
print("En sirkel med radius 7 har areal", None)