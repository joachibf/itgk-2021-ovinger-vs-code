# Øving 1

## Læringsmål:

* Kunne bruke vanlige regneoperatorer i Python: addisjon + , subtraksjon – , multiplikasjon * , divisjon / , heltallsdivisjon '// , modulo % og potens **
* Forstå forskjellen på de fire elementære datatypene: heltall (int), flyttall (float), tekststrenger (str) og sannhetsverdier (boolean)
* Skjønne hvordan regneoperatorene virker for de ulike datatypene
* Kunne bruke vitenskapelig notasjon for spesielt små og spesielt store flyttall
* Forstå at kode må være uten feil for å kjøre, og kunne rette enkle syntaksfeil
* Forstå hvorfor og hvordan vi bruker variable i programmering, og at variabeltilordning ikke er det samme som matematisk likhet
* Kunne skrive ut til skjerm og lese inn data fra brukeren via tastaturet med standardfunksjonene print() og input()
* Kunne utføre enkel konvertering mellom datatyper med standardfunksjoner som int() og float()

## Starting Out with Python:

* Kap. 2: Input, Processing, and Output


 

## Godkjenning:

For å få godkjent øvingen må du gjøre Intro til Jupyter oppgaven og **4** av 10 andre oppgaver. 

Oppgavene er markert med vanskelighetsgrad. Oppgaver som går forbi det som er blitt forelest er markert med "Vanskelig".

Alle oppgavene skal demonstreres til en læringsassistent på sal. I oppgaver der du skriver programkode skal også denne vises fram. Lykke til!


Oppgave | Tema | Vanskelighetsgrad
--- | --- | ---
[Jeg elsker ITGK!](Lett/Jeg%20elsker%20ITGK.md) | Output | Lett
[Kalkulasjoner](Lett/Kalkulasjoner.md)|Matteoperasjoner| Lett
[Input og variable](Lett/Input%20og%20variable.md)|Input/output og variabler| Lett
[Tallkonvertering](Lett/Tallkonvertering.md)|Konvertering| Lett
[Peppes pizza](Lett/Peppes%20Pizza.md)|Matteoperasjoner| Lett
[Geometri](Lett/Geometri.md)|Debugging| Lett
[Vitenskapelig notasjon](Middels/Vitenskapelig%20notasjon.md)|konvertering|Middels
[Tetraeder](Middels/Tetraeder.md)|innebygde funksjoner|Middels
[Bakekurs](Middels/Bakekurs.md)|Formatering|Middels
[James Bond and Operation round](Vanskelig/James%20Bond.md)|Strenger, avrunding, heltallsdivisjon, modulo|Vanskelig