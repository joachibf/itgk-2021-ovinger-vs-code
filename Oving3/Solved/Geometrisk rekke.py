import sys
print("Geometrisk rekke!")
print("______________________ \n")

print("Oppgave a)")
#TODO løs oppgave a her:
r = float(0.5)
n = 4
i = 0
sum = 0
while i <= n:
    sum += r**i
    i+=1
print(sum)


print("Oppgave b og c)")
#TODO løs oppgave b og c her:
n = float('inf')
tol = 0.001
i = 0
sum = 0
lim = 1/(1-r)
while i <= n:
    sum += r**i
    i+=1
    if (lim-sum) <= tol:
        print("Sum reached within tolerance of {0} limit {1} at iteration {2}".format(tol,lim,i))
        print("Difference between sum and limit was", (lim-sum))
        break
    
print("Sum:", sum)
