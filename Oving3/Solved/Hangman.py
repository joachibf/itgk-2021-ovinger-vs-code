print("HANGMAN!")
print("______________________ \n")
import re
#TODO løs oppgave a,b c(frivillig) her:
word = input("Secret word: ")
word.lower()
numlives = int(input("Number of lives:(must be an integer) "))
guessed = False
guessed_list = []
def winCheck(word, charlist):
    for c in word:
        if c not in charlist:
            return False
    return True

def generateMaskedWord(word, charlist):
    masked = ""
    for c in word:
        if c in charlist:
            masked+=c
        else:
            masked+='*'
    return masked

while not guessed and numlives>0:
    if(len(guessed_list)>0):
        print("Already guessed letters: ", guessed_list)
        print("Current number of lives: ", numlives)
        print("Word hint:", generateMaskedWord(word, guessed_list))
    letter = input("\n\nInput letter guess: ")
    letter.lower()
    if not re.match("^[a-z]{1}$", letter):
        print("Please only input a single a-z character at a time")
    elif letter in guessed_list:
        print("Letter already guessed. Try again")
    elif letter not in word:
        print("The letter is not in the word")
        numlives-=1
        if numlives <= 0:
            print("You ran out of lives and lost.")
            break
        print("Your lives are now reduced to: ", numlives)
        guessed_list.append(letter)
    elif letter in word:
        print("Correct!, The letter is in the word!")
        guessed_list.append(letter)
        if winCheck(word, guessed_list):
            print("You won")
            print("The word was", word, "Your guesses were", guessed_list)
            break
        else:
            print("There are still letters remaining.")

        

