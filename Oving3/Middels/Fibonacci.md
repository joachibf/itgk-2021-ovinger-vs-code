### **[Intro øving 3](../Intro_Øving3.md)**
<br>

# Fibonacci

**Læringsmål:**

* Løkker

**Starting Out with Python:**

* Kap. 4.3

<br>

## OPPGAVER

Alle deloppgaver skal besvares her: *[Fibonacci.py](Fibonacci.py)* !
<br>

I denne oppgaven skal du bruke løkker til å regne ut fibonaccitall.  
Fibonaccitallene er definert som følger: 

* **f(0)=0**
* **f(1)=1**
* **f(k)=f(k−1)+f(k−2)**

Det vil si at de to første tallene i rekken er 0 og 1, deretter er det neste tallet summen av de to forrige tallene. Starten på rekken ser derfor slik ut: 0 1 1 2 3 5 8 13 ...

## a) 
Lag et program som regner ut og returnerer det k-te fibonaccitallet f(k) ved hjelp av iterasjon. Har du gjort det rett skal det 10-ende Fibonaccitallet bli 34. **Husk at det første tallet i rekken er tall nummer 0**

## b)
Skriv om programmet i deloppgave a) slik at det også regner ut summen av alle fibonaccitallene. Har du gjort det rett det rett skal summen av Fibonaccitallene opp til 10 bli 88.

## c) Frivillig, vanskeligere oppgave
Modifiser programmet til å returnere en liste med alle fibonaccitallene opp til og med f(k).

Husk å skrive ut svarene til skjerm.