### **[Intro øving 3](../Intro_Øving3.md)**
<br>

# Hangman

**Læringsmål:**

* Betingelser
* While-løkker

**Starting Out with Python:**

* Kap. 3.4 
* Kap. 4.2

<br>

## OPPGAVER

Alle deloppgaver besvares her: *[Hangman.py](Hangman.py)* !


Kapittel fire introduserer repetisjonsstrukturer. Dette er en mye brukt programmeringsteknikk som brukes når et program skal utføre den samme oppgaven mange ganger etter hverandre. I denne oppgaven bruker vi en enkel while-løkke for å lage et hangman-spill i Python. 

Lag et program som tar inn et ord (tekststreng) og antall liv (heltall) fra en bruker, og lar en annen (eller samme) bruker gjette på bokstaver i ordet. 

## a)
Start med å hente inn data fra bruker. Lagre dette i to variabler "hemmelig_ord" og "antall_liv".

<br>

## b)
Under vises skallet til en while-løkke som kjører evig. Din oppgave er å utvide programmet fra **a)** ved å fylle inn manglende logikk inne i løkken. Ting som må gjøres er:

* Hent inn en bokstav fra bruker
* Sjekk om denne er i det hemmelige ordet 
  * Trekk fra et liv dersom brukeren tipper feil
 * Hvis brukeren ikke har flere liv skal løkken avsluttes (HINT: bruk "break" for å avslutte en løkke) 

```python
while True:
    # do something
```

PS: Husk å skrive ut resultatet til brukeren. 

<br>

**Eksempel på kjøring av kode:**


*Skriv inn det hemmelige ordet:* hemmelig <br>
*Hvor mange forsøk får brukeren?* 2 <br>
*Gjett på én bokstav i ordet:* f <br>
*Bokstaven f er ikke i ordet. <br>
Du har  1 liv igjen, prøv på nytt. <br>
Gjett på én bokstav i ordet:* h <br>
*Stemmer, bokstaven er i ordet <br>
Gjett på én bokstav i ordet:* e <br>
*Stemmer, bokstaven er i ordet <br>
Gjett på én bokstav i ordet:* r <br>
*Bokstaven r er ikke i ordet. <br>
Du har ingen liv igjen.* <br>


<br>

## c) FRIVILLIG vanskeligere oppgave
Fyll inn logikk for å fullføre spillet. Ting som kan implementeres er:

* Avslutt løkken hvis brukeren har gjettet alle bokstavene i løsningsordet.
* For hvert gjett, print ut maskert løsningsord med stjerner for hver bokstav som fortsatt ikke er gjettet. `(Eks.: lø*ning*ord)`

Du kan utvide koden fra **a** og **b**.