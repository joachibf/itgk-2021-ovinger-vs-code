print("Alternerende sum!")
print("______________________ \n")

print("Oppgave a)")
#TODO løs oppgave a her:
num = int(input("Input your number: "))
sum = 0
for i in range(0,num+1):
    if i%2==0:
        sum-=i**2
    else:
        sum+=i**2
print(sum)


print("Oppgave b)")
#TODO løs oppgave b her:
k = int(input("Max sum for iteration? "))
sum = 0
for i in range(0,num+1):
    if i%2==0:
        sum-=i**2
    else:
        temp_sum =sum+i**2
        if temp_sum>k:
            print(f"Sum passed given value k:{k} at iteration i:{i}\nSum before this iteration was {sum}")
        else:
            sum+=i**2
print(f"Max not reached. End sum value: {sum}")
