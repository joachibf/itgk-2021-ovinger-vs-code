### **[Intro øving 3](../Intro_Øving3.md)**
<br>


# Alternerende sum

**Læringsmål:**

* Løkker

**Starting Out with Python:**

* Kap. 4.2-4.3

**OPPGAVER**

Alle deloppgaver skal besvares her: *[Alternerende sum.py](Alternerende%20sum.py)* !

I denne oppgaven skal du ved hjelp av løkker summere sammen tall, basert på brukerinput. 

## a)
Skriv et program som leser inn et heltall n fra bruker og legger sammen tallserien under.

**1<sup>2</sup> - 2<sup>2</sup> + 3<sup>2</sup> - 4<sup>2</sup> + 5<sup>2</sup> - . . . +/- n<sup>2**

Legg merke til at alle partallene har negativt fortegn og alle oddetallene har positivt fortegn. Husk at navnet på variabelen din ***ikke*** kan være **sum**, ettersom dette er navnet på en funksjon i python. Husk også at range() bare går til et tall og ikke til og med.

## b)
Skriv et nytt program slik at det avslutter iterasjonen **før** summen av tallene er større enn det positive heltallet k. Dette vil si at resultatet som skal skrives ut er summen på siste ledd før summen går over tallet k.

Hold styr på hvor mange ledd fra tallserien som er brukt i summen og skriv dette ut sammen med resultatet.



