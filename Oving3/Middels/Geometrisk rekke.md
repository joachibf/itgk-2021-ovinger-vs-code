### **[Intro øving 3](../Intro_Øving3.md)**
<br>

# Geometrisk rekke

**Læringsmål:**

* Løkker

**Starting Out with Python:**

* Kap. 4.2-4.3

<br>


## OPPGAVER

Alle deloppgaver besvares her: *[Geometrisk rekke.py](Geometrisk%20rekke.py)* !


En geometrisk rekke er en sum som kan skrives på formen under: 

$\sum_{i=0}^{n}r^{i}=r^{0}+r^{1}+r^{2}+r^{3}+\cdot \cdot \cdot +r^{n}$  
$r \in (-1,1)$


## a)

Lag et program som summerer en geometrisk rekke fra 0 til n ved hjelp av en while løkke.

Sjekk: r = 0.5 og n = 4 skal gi sum = 1.9375

<br>

## b)

Skriv om programmet ditt slik at løkken avsluttes når din sum er innenfor en toleranse tol til grenseverdien av summen.

Test dette med tol = 0.001, r = 1/2, som vist i eksempelet under.

**Hint:** <br>
Summen til en geometrisk rekke er $\frac{1-r^{n+1}}{1-r}$
. For rekken i b) blir grenseverdien $\frac{1}{1-r}$ = 2.

<br>

## c)

La programmet gi ut antall iterasjoner som trengs for å være innenfor toleransen. Gi også ut den virkelige differansen mellom summen du fant og grenseverdien.

<br>

**Eksempel på kjøring av kode a, b, og c:**

a)  
*Summen av rekken er 1.9375*
 
b & c)  
*For å være innenfor toleransegrensen: 0.001 , kjørte løkken  11 ganger.  <br>
Differansen mellom virkelig og estimert verdi var da 0.0009765625*
