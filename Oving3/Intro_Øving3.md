**Øving 3**

**Læringsmål:**

* Løkker
* Betingelser
* Strenger
* Datamaskiners virkemåte

**Starting Out with Python:**

* Kap. 4: Repetition Structures

**Theory book:**

* Kap. 9: Principles of Computer Operations
 

**Godkjenning:**

For å få godkjent øvingen må du gjøre 4 av 11 andre oppgaver. ***1*** av de ***4*** må være en oppgave med vanskelighetsgrad middels.

Oppgavene er markert med vanskelighetsgrad. 

Alle oppgavene skal demonstreres til en læringsassistent på sal. I oppgaver der du skriver programkode skal også denne vises fram. Lykke til!

Alle oppgavene skal demonstreres til en læringsassistent på sal. I oppgaver der du skriver programkode skal også denne vises fram. Lykke til!

Oppgave | Tema | Vanskelighetsgrad
--- | --- | ---
[Introduksjon til løkker](Lett/Intro%20til%20løkker.md) |Løkker | Lett
[Mer om løkker](Lett/Mer%20om%20løkker.md)|Løkker| Lett
[Intro til nøstede løkker](Lett/Nøstede%20løkker.md)|Nøstede løkker| Lett
[Kodeforståelse](Lett/Kodeforståelse.md)|Kodeforståelse| Lett
[Gjett tallet](Lett/Gjett%20tallet.md)|Løkker og betingelse| Lett
[Tekstbasert Spill 2](Middels/Tekstbasert%20spill%202.md)|Løkker og betingelse|Middels
[Geometrisk rekke](Middels/Geometrisk%20rekke.md)|Løkker og betingelse|Middels
[Fibonacci](Middels/Fibonacci.md)|Løkker|Middels
[Alternerende sum](Middels/Alternerende%20sum.md)|Løkker|Middels
[Hangman](Middels/Hangman.md)|Løkker| Middels
[Doble løkker](Middels/Doble%20løkker.md)|Nøstede Løkker| Middels
