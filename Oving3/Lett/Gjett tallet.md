### **[Intro øving 3](../Intro_Øving3.md)**
<br>


# Gjettelek

**Læringsmål:**

* Løkker
* Betingelser
* Innebygde funksjoner

**Starting Out with Python:**

* Kap. 4.2-4.3

## OPPGAVER

Alle deloppgaver besvares her: *[Gjett tallet.py](Gjett%20tallet.py)* !
<br><br>
I denne oppgaven skal du lage et program som genererer et tilfeldig heltall i et gitt intervall, og deretter lar brukeren gjette på hvilket tall dette er. Dette bør gjøres ved bruk av løkker.

I denne oppgaven må du benytte deg av biblioteket random. Derfor skal koden din starte med `import random`.

Eksempel på bruk av random.randint()

```python
tilfeldigTall = random.randint(1,50)    #gir deg et tilfeldig tall fra og med 1, til og med 50
tall = random.randint(a,b)              #gir deg et tilfeldig tall mellom a og b. (Fra og med a, til og med b)
```

## a)
Be brukeren om å velge en nedre og en øvre grense for tall han eller hun skal gjette på. Lagre disse to opplysningene i to variabler.

<br>

## b)
Lag en variabel `TilfeldigTall` som genererer et tilfeldig tall i intervallet mellom den øvre og den nedre grensen som brukeren har satt. Fortsett å skriv i samme kode som i oppgave a.

<br>

## c)
Skriv en while-løkke som kjører så lenge brukeren ikke har gjettet riktig svar. Brukeren skal få tilbakemelding for hvert gjett om han eller hun gjettet for lavt, for høyt eller riktig. Fortsett å skriv i samme kode som i oppgave a og b.

**Eksempel på kjøring:**

*Gi en nedre grense for det tilfeldige tallet:* 1 <br>
*Gi en øvre grense for det tilfldige tallete:* 100 <br>
*Make a guess:* 50 <br>
*The correct number is lower <br>
Make a guess:* 25 <br>
*The correct Number is higher <br>
Make a guess:* 37 <br>
*The correct number is lower <br>
Make a guess:* 32 <br>
*You guessed correct!* <br>
