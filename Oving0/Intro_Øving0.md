# Øving 0

## Læringsmål:

**Her vil det stå læringsmålene den gitte øvingen dekker!**

Eks:

* Kunne bruke den innebygde funksjonen print().

## Starting Out with Python:
**Her står det hvilke kapitler i boken som er relevant for den gitte øvingen!**

Eks:
* Kap. 2: Input, Processing, and Output

<bR>


 

## Godkjenning:

**Her vil det alltid stå hva som kreves for å bestå øvingen** Det kan variere fra øving til øving så les nøye. 

Oppgavene er markert med vanskelighetsgrad. Oppgaver som går forbi det som er blitt forelest er markert med "vanskelig".

Alle oppgavene skal demonstreres til en læringsassistent på sal. I oppgaver der du skriver programkode skal også denne vises fram. Lykke til!


Oppgave | Tema | Vanskelighetsgrad
--- | --- | ---
[Hello world!](Lett/Hello%20world!.md) | Output | Lett

<br>

For å navigere mellom oppgaver anbefaler jeg å trykke på linken i introfilene (slik som denne!) Her vill det vært "Hello world!" over. 

Det andre alternativet vil være å navigere gjennom øvingene i sidemenyen. 